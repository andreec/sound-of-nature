<?php

//
// Application Environment
//
// -- Debug
define('DEBUG', true);
// -- Timezone
date_default_timezone_set('Asia/Jakarta');
// -- Path
define('PATH_WWW', dirname(__FILE__));
define('PATH_APP', PATH_WWW . '/app');
// -- Include
set_include_path(get_include_path() . PATH_SEPARATOR . PATH_APP . '/lib');

//
// DEBUG
//
if(DEBUG) {
	ini_set('display_errors', 1); 
	ini_set('log_errors', 0);
	error_reporting(E_ALL);
}
else {
	ini_set('display_errors', 0); 
	ini_set('log_errors', 1);
	error_reporting(E_ERROR);
}

require PATH_APP.'/lib/Database.php';
require PATH_APP.'/model/User.php';
require PATH_APP.'/model/Submission.php';
require PATH_APP.'/model/Listener.php';

//
// Execute App
//
require PATH_APP . '/lib/Fat.php';
$app = Fat::app(PATH_APP . '/config.inc.php');

$app->run();
