<?php

return array(
    'debug' => DEBUG,
    // -- PATH
    'path.www'    => PATH_WWW,
    'path.app'    => PATH_APP,
    'path.lib'    => PATH_APP . '/lib',
    'path.cache'  => PATH_APP . '/cache',
    'path.hook'   => PATH_APP . '/hook',
    'path.action' => PATH_APP . '/action',
    'path.view'   => PATH_APP . '/view',

    // -- DATABASE
    'db.host'   => 'localhost',
    'db.name'   => 'sound_of_nature',
    'db.user'   => 'root',
    'db.pass'   => '',
    'db.prefix' => '',

    // -- FACEBOOK
    'fb.appid'  => '1504888909760625',
    'fb.secret' => 'a2053b02ad2cb7335a4d0a6c39854ce4',
    'fb.upload' => false,
    // -- MAIL

    // -- APPLICATION, xituz.sandbox was default
    'fb.pageid' => '289058697815580',
    
    // -- Uploads
    'file.uploads' => 'uploads/'
);
