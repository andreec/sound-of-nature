<?php

class UserTest extends \PHPUnit_Framework_TestCase {
    
    private $db;
    private $user;
    
    public function setUp() {
        $this->db = new Database(include PATH_APP.'/config.inc.php');
        $this->user = new User();
    }
    
    public function testRetrieve() {
        $user = $this->user->retrieve(1);
        $this->assertEquals('Edward', $user['name']);
        
        $user = $this->user->retrieve(2);
        $this->assertEquals('Crazenezz', $user['name']);
    }
    
    public function testRegister() {
        $this->assertNotNull($this->db->config('db.host'));
        $this->assertEquals('localhost', $this->db->config('db.host'));
    }
    
}
?>
