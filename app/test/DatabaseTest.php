<?php

class DatabaseTest extends \PHPUnit_Framework_TestCase {
    
    private $db;
    
    public function setUp() {
        $this->db = new Database(include PATH_APP.'/config.inc.php');
    }
    
    public function testConfig() {
        $this->assertNotNull($this->db->config('db.host'));
        $this->assertEquals('localhost', $this->db->config('db.host'));
    }
    
    public function testConnection() {
        $this->assertNotNull($this->db->pdo());
    }
    
}
?>
