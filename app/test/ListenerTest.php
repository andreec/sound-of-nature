<?php

class ListenerTest extends \PHPUnit_Framework_TestCase {
    
    private $db;
    private $listener;
    
    public function setUp() {
        $this->db = new Database(include PATH_APP.'/config.inc.php');
        $this->listener = new Listener();
    }
    
    public function testExists() {
        $listener = $this->listener->isExists(26, 8);
        $this->assertTrue($listener);
    }
    
    public function testInsert() {
        $this->listener->insert(array(
            'user_id' => 27,
            'user_submission_id' => 8
        ));
        
        $listener = $this->listener->isExists(27, 8);
        $this->assertTrue($listener);
    }
    
}
?>
