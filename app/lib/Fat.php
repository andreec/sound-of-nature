<?php

class Fat
{
	// HTTP Response Code
	public static $status = array(
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',

		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		307 => 'Temporary Redirect',

		400 => 'Bad Request',
		401 => 'Unauthorized',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',

		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported'
	);

	public static function app($config)
	{
		if(is_string($config) && is_file($config)) {
			return new FatApp(include $config);
		} elseif(is_array($config)) {
			return new FatApp($config);
		}
		throw new FatException('Config parameter is invalid.');
	}

	public static function module($name, $config = array())
	{
		// TODO:Finalise module factory pattern
	}
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class FatApp
{
	public function __construct($config)
	{
		$this->__config = $config;
	}



	/**
	 * Application Config
	 */
	private $__config;
	function config($key)
	{
		if(isset($this->__config[$key])) {
			return $this->__config[$key];
		}
		return null;
	}
	
	/**
	 * Request
	 */
	// Full server path with no trailing slash to currently running script or
	// bootstrap file directory, such as index.php directory.
	private $__basePath = null;
	public function basePath($basePath = null)
	{
		if($basePath === null) {
			if($this->__basePath === null) {
				$this->__basePath = dirname($_SERVER['SCRIPT_FILENAME']);
			}
			return $this->__basePath;
		}
		if(!file_exists($basePath) || !is_dir($basePath)) {
			return false;
		}
		$this->__basePath = $basePath;
		return true;
	}
	// Full URL to currently running script or bootstrap file directory with
	// trailing slash, such as index.php directory. Useful for <base href="" />
	// usage on HTML layout.
	private $__baseHref = null;
	public function baseHref($baseHref = null)
	{
		if($baseHref === null) {
			if($this->__baseHref === null) {
				$this->__baseHref = $this->schemeUrl() . '://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['SCRIPT_NAME']), '/') . '/';
			}
			return $this->__baseHref;
		}
		$this->baseHref = rtrim($baseHref, '/') . '/';
		return true;
	}
	// Base URL path or root path of currently running script or bootstrap file
	// with leading slash and without trailing slash. Default to empty string
	// for not having sub directory.
	private $__baseUrl = null;
	public function baseUrl($baseUrl = null)
	{
		if($baseUrl === null) {
			if($this->__baseUrl === null) {
				$requestUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
				$scriptName = $_SERVER['SCRIPT_NAME'];
				$baseUrl = strpos($requestUri, $scriptName) === 0 ? $scriptName : str_replace('\\', '/', dirname($scriptName));
				$this->__baseUrl = rtrim($baseUrl, '/');
			}
			return $this->__baseUrl;
		}
		$this->__baseUrl = '/' . trim($baseUrl);
		return true;
	}
	// Path URL or path info after the base url or root path of currently
	// running script or bootstrap file with leading slash and without trailing
	// slash. Default to slash for root or index.
	private $__pathUrl = null;
	public function pathUrl($url = null)
	{
		if($url === null) {
			if($this->__pathUrl === null) {
				if(!empty($_SERVER['PATH_INFO'])) {
					$this->__pathUrl = $_SERVER['PATH_INFO'];
				}
				else {
					if(isset($_SERVER['REQUEST_URI'])) {
						$this->__pathUrl = parse_url($this->schemeUrl() . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], PHP_URL_PATH);
					}
					else if(isset($_SERVER['PHP_SELF'])) {
						$this->__pathUrl = $_SERVER['PHP_SELF'];
					}
					else {
						throw new FatException('Unable to detect request URI');
					}
				}
				if($this->baseUrl() !== '' && strpos($this->__pathUrl, $this->baseUrl()) === 0) {
					$this->__pathUrl = substr($this->__pathUrl, strlen($this->baseUrl()));
				}
				$this->__pathUrl = '/' . trim($this->__pathUrl, '/');
			}
			return $this->__pathUrl;
		}
		$this->__pathUrl = '/' . trim($url, '/');
		return true;
	}
	// URL Scheme to determine whether content is being transmited encrypted or
	// not with available value: http | https.
	private $__schemeUrl = null;
	public function schemeUrl($scheme = null) {
		if($scheme === null) {
			if($this->__schemeUrl === null) {
				$this->__schemeUrl = (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === 'off') ? 'http' : 'https';
			}
			return $this->__schemeUrl;
		}
		$scheme = strtolower($scheme);
		if(in_array($scheme, array('http', 'https'))) {
			$this->__schemeUrl = $scheme;
			return true;
		}
		return false;
	}
	// Is current request an AJAX request.
	private $__xhr = null;
	public function xhr($xhr = null)
	{
		if($xhr === null) {
			if($this->__xhr === null) {
				$this->__xhr = !empty($_SERVER) && !empty($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] === 'XMLHttpRequest';
			}
			return $this->__xhr;
		}
		$this->__xhr = $xhr;
	}
	// REST Method
	private $__method = null;
	public function method($method = null)
	{
		if($method === null) {
			if($this->__method === null) {
				$this->__method = getenv('REQUEST_METHOD') ?: 'GET';
			}
			return $this->__method;
		}
		$this->__method = $method;
	}
	// Raw Parameters
	public function __get($name)
	{
		switch($name) {
			case 'get':
				$this->get = &$_GET;
				break;
			case 'post':
				$this->post = &$_POST;
				break;
			case 'file':
				$this->get = &$_FILES;
				break;
			case 'cookie':
				$this->get = &$_COOKIE;
				break;
			case 'session':
				$this->get = &$_SESSION;
				break;
			case 'raw':
				$this->raw = file_get_contents('php://input');
				break;
			default:
				throw new FatException("Unsupported application parameters.");
		}
	}



	/**
	 * Response
	 */
	private $__type = 'html';
	private $__status = 200;
	private $__headers = array();
	private $__vars = array();
	private $__bodies = array();
	private $__views = array();
	public function redirect($url, $status = '302')
	{
		$url = trim($url);
		if(strpos($url, 'http://') !== 0 && strpos($url, 'https://') !== 0 ) {
			$url = trim($url, '/');
			if(in_array($url, array('401', '404', '500'))) {
				$url = $url . "?referrer=" . $this->pathUrl();
			}
			$url = $this->baseHref() . $url;
		}
		if(!isset(Fat::$status[$status])) {
			throw new FatException("Redirect response code ({$status}) is invalid.");
		}
		header("HTTP/1.0 {$status} " . Fat::$status[$status]);
		header('Location: ' . $url, true, $status);
		die();
	}



	/**
	 * Controller & Response
	 */
	public function run()
	{
		try {
			$pathHook = $pathAction = $pathView = str_replace('..', '', $this->pathUrl());

			// Process Hook
			/*
			$params = explode('/', trim($pathHook, '/'));
			$pathHook = '';
			foreach($params as $param) {

			}
			do {
				$this->_hook($pathHook, $params);

				$pos = strpos($pathHook, '/', 1);
				if(($param = substr($pathHook, $pos + 1)) !== false) {
					array_unshift($params, $param);
				}
				$pathHook = substr($pathHook, 0, $pos);
			} while($pathHook != '');
			*/

			// Process Action
			$params = array();
			do {
				if($this->_action($pathAction, $params)) {
					return;
				}
				$pos = strrpos($pathAction, '/');
				if(($param = substr($pathAction, $pos + 1)) !== false) {
					array_unshift($params, $param);
				}
				$pathAction = substr($pathAction, 0, $pos);
			} while($pathAction != '');

			// Process View
			$view = $this->_view($pathView, array(
				self::PARAM => $params
			));
			if($view !== false) {
				echo $view;
				return;
			}

			// Process Index Action/View
			$vars = array(
				self::PARAM => $params,
			);
			if(!$this->_action('/index', $params)) {
				$view = $this->_view('/index', $vars);
				if($view) {
					echo $view;
					return;
				}
				// Process 404 Action/View
				if(!$this->_action('/404', $params)) {
					$view = $this->_view('/404', $vars);
					if($view) {
						echo $view;
						return;
					}
					die('404 action/view required!');
				}
			}
		} catch(Exception $e) {
			// Process 500 Action/View
			$vars = array(
				self::ERROR => $e,
			);
			if(!$this->_action('/500', $params)) {
				$view = $this->_view('/500', $vars);
				if($view) {
					echo $view;
					return;
				}
				die($e);
			}
		}
	}
	// HOOK: Allowed Return Value
	//
	// 1. 
	protected function _hook()
	{
		// TODO: Implement hook system
	}
	// ACTION: Allowed Return Value
	//
	// 0. Null      : Skip current action.
	// 1. False     : No need to render view, instead send output buffer.
	// 2. True      : Render view under the same path, if available.
	// 3. Integer   : Render view under the same path, with custom header response code.
	// 4. Array()   : Render view under the same path, with returned value as parameters.
	// 5. String    : Output returned string as body.
	// 6. View Path : Render view using the path returned, string path need to start with a slash.
	// 7. Object()  : Render views in a hierarchical order, with returned value as parameters.
	//                type    : Possible values (json, jtext, xml, download, image).
	//                headers : Valid HTTP response header value.
	//                status  : Valid HTTP response code, ex: 200, 401, 404, 500
	//                vars    : View variables.
	//                views   : Hierarchical views rendering with possible for master -> subviews, ex:
	//                          array(
	//                              '/layout',                                 // 1. Master Layout
	//                              array('/part/header', '/part/footer'),     // 2. Sub Layout
	//                              array('/widget1', '/widget2', '/widget3'), // 3. View Parts
	//                          );
	const JSON     = 'JSON';
	const JTEXT    = 'JTEXT';
	const XML      = 'XML';
	const HTML     = 'HTML';
	const DOWNLOAD = 'DOWNLOAD';
	protected function _action($path, $params)
	{
		$actionFile = $this->__config['path.action'] . $path . '.php';
		if(is_file($actionFile)) {
			// Execute Action File
			ob_start();
			$action = include $actionFile;
			$output = ob_get_contents();
			ob_end_clean();

			// Process Return Value (VIEW)
			$view = false;
			// 0. Null
			if($action === null) {
				return false;
			}
			// 1. False
			elseif($action === false) {
				$view = $output;
			}
			// 2. True
			elseif($action === true) {
				$view = $this->_view($path, array(
					self::PARAM  => $params,
					self::OUTPUT => $output,
				));
			}
			// 3. Integer
			elseif(is_int($action) && isset(Fat::$status[$action])) {
				header("HTTP/1.0 {$action} " . Fat::$status[$action]);
				$view = $this->_view($path, array(
					self::PARAM  => $params,
					self::OUTPUT => $output,
				));
			}
			// 4. Array()
			elseif(is_array($action)) {
				$action[self::PARAM]  = $params;
				$action[self::OUTPUT] = $output;
				$view = $this->_view($path, $action);
			}
			elseif(is_string($action)) {
				// 5. String
				if(strpos($action, '/') !== 0) {
					$view = $action;
				}
				// 6. View Path
				else {
					$view = $this->_view($action, array(
						self::PARAM  => $params,
						self::OUTPUT => $output,
					));
				}
			}
			// 7. Object
			elseif(is_object($action)) {
				// -- Process Variables
				if(!isset($action->vars) || !is_array($action->vars)) {
					$vars = array();
				} else {
					$vars = $action->vars;
				}
				$vars[self::PARAM]  = $params;
				$vars[self::OUTPUT] = $output;
				// -- Send Custom Headers & Status
				if(!empty($action->type)) {
					switch($action->type) {
						case self::JSON:
							$_SERVER["HTTP_X_REQUESTED_WITH"] = 'XMLHttpRequest';
							header('Cache-Control: no-cache, must-revalidate');
							header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
						    header('Content-type: application/json; charset=utf-8');
							echo json_encode($vars[0]);
							return true;
						case self::JTEXT:
							/*
							@unset($vars[self::BODY]);
							@unset($vars[self::PARAM]);
							@unset($vars[self::ERROR]);
							@unset($vars[self::OUTPUT]);
							@unset($vars[self::DEBUG]);
							*/
							header('Cache-Control: no-cache, must-revalidate');
							header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
							if($action->type == self::JTEXT && !$this->xhr()) {
								header('Content-type: text/html; charset=utf-8');
								echo '<textarea>' . json_encode($vars) . '</textarea>';
							} else {
								header('Content-type: application/json; charset=utf-8');
								echo json_encode($vars);
							}
							return true;
						/*
						case self::XML:
							header('Cache-Control: no-cache, must-revalidate');
							header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
							header('Content-type: text/xml; charset=utf-8');
							// TODO: Implement XML rendering.
							//$view = json_encode($vars);
							break;
						case self::DOWNLOAD:
							header('Content-type: application/pdf');
							header('Content-Disposition: attachment; filename="downloaded.pdf"');
							readfile('original.pdf');
							// TODO: Implement download
							break;
						*/
						default:
							throw new FatException("Unsupported output type ({$action->type}).");
					}
				}
				// -- Send Headers
				if(!empty($action->headers) && is_array($action->headers)) {
					foreach($action->headers as $header) {
						header($header);
					}
				}
				// -- Send Status
				if(!empty($action->status) && is_int($action->status)) {
					if(!isset(Fat::$status[$action->status])) {
						throw new FatException("Response code ({$status}) is invalid.");
					}
					header("HTTP/1.0 {$action->status} " . Fat::$status[$action->status]);
				}
				// -- Render Views
				if(!empty($action->views)) {
					if(!is_array($action->views)) {
						$views = array((string) $action->views);
					} else {
						$views = $action->views;
					}
					$views_length = count($views);
					for($i = $views_length - 1; $i >= 0; $i--) {
						$paths = $views[$i];
						if(!is_array($paths)) {
							$paths = array((string) $paths);
						}
						$bodies = array();
						foreach($paths as $id => $path) {
							$bodies[$id] = $this->_view($path, $vars);
						}
						if($i === 0) {
							$view = implode($bodies);
						} else {
							$vars[self::BODY] = $bodies;
						}
					}
				}
			}
			// Unknown
			else {
				throw new FatException("Unknown return value from action.");
			}
			if($view) {
				echo $view;
			}
			return true;
		}
		return false;
	}
	// VIEW: Possible Generated Variables
	//
	// 1. BODY (array)      : Rendered sub views, if there is sub views.
	// 2. PARAM (array)     : Request path url parameters, after matching action path.
	// 3. ERROR (Exception) : Catched exception in a try catch block.
	// 4. OUTPUT (string)   : Buffered output generated by action.
	// 5. DEBUG (array)     : Server parameters for debuging, disabled when debug set to false.
	const BODY   = 'BODY';
	const PARAM  = 'PARAM';
	const ERROR  = 'ERROR';
	const OUTPUT = 'OUTPUT';
	const DEBUG  = 'DEBUG';
	protected function _view($path, $vars = array())
	{
		$viewFile = $this->__config['path.view'] . $path . '.php';
		if(is_file($viewFile)) {
			if(!is_array($vars)) {
				$vars = array($vars);
			}
			if(!isset($vars['base_href'])) {
				$vars['base_href'] = $this->baseHref();
			}
			ob_start();
			extract($vars);
			include $viewFile;
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}
		return false;
	}
	
	public function getView($path, $vars = array()) {
	    return $this->_view($path, $vars);
	}



	/**
	 * Database Module
	 */
	private $__pdo;
	public function pdo()
	{
		if(!isset($this->__pdo)) {
			$this->__pdo = new PDO(
				"mysql:host=" . $this->config('db.host')/* . ";port:" . $this->config('db.port')*/ . ";dbname=" .  $this->config('db.name') . ";",
				$this->config('db.user'), $this->config('db.pass')
			);
		}
		return $this->__pdo;
	}
	
	private $__orm;
	public function orm()
	{
		if(!isset($this->__orm)) {
			require $this->config('path.lib') . '/NotORM.php';
			$this->__orm = new NotORM($this->pdo(), new NotORM_Structure_Convention(
				'id', // Primary
				'%s_id', // Foreign
				'%s', // Table
				$this->config('db.prefix') // Prefix
			));
		}
		return $this->__orm;
	}
	


	/**
	 * Facebook Module
	 */
	private $__fb;
	public function fb()
	{
		if(!isset($this->__fb)) {
			require $this->config('path.lib') . '/facebook-php-sdk-3.2.3/facebook.php';
			$this->__fb = new Facebook(array(
				'appId'      => $this->config('fb.appid'),
				'secret'     => $this->config('fb.secret'),
				'fileUpload' => $this->config('fb.upload'),
			));
		}
		return $this->__fb;
	}
	private $__fb_user;
	public function fbUser()
	{
		if($this->fbIsConnected()) {
			return $this->__fb_user;
		}
		return null;
	}
	private $__fb_data;
	public function fbData()
	{
		if($this->fbIsConnected()) {
			return $this->__fb_data;
		}
		return null;
	}
	private $__fb_is_connected;
	public function fbIsConnected()
	{
		if(isset($this->__fb_is_connected)) {
			return $this->__fb_is_connected;
		}
		$this->__fb_user = $this->fb()->getUser();
		if($this->__fb_user) {
			try {
				$this->__fb_data = $this->fb()->api('/me');
			} catch (FacebookApiException $e) {
				error_log($e);
				$this->__fb_user = null;
				$this->__fb_data = null;
			}
		}
		$this->__fb_is_connected = $this->__fb_user ? true : false;
		return $this->__fb_is_connected;
	}
	private $__fb_in_page;
	public function fbInPage()
	{
		if(isset($this->__fb_in_page)) {
			return $this->__fb_in_page;
		}

		$this->__fb_in_page = false;
		if($fbdata = $this->fb()->getSignedRequest()) {
			if(!empty($fbdata) && is_array($fbdata)
				&& !empty($fbdata['page']) && is_array($fbdata['page'])
				&& !empty($fbdata['page']['id'])
			) {
				$this->__fb_in_page = $fbdata['page']['id'];
			}
		}
		return $this->__fb_in_page;
	}
	public function fbInPageLiked()
	{
		if($this->fbInPage()) {
			$fbdata = $this->fb()->getSignedRequest();
			return $fbdata['page']['liked'];
		}
		return false;
	}
	public function fbIsPageLiked($page_id)
	{
		if(empty($page_id)) {
			return false;
		}
		$result = $this->fb()->api(array(
			'method' => 'fql.query',
			'query'  => "SELECT page_id FROM page_fan WHERE uid=me() AND page_id = {$page_id}",
		));
		return !empty($result);
	}



	/**
	 * Email Module
	 */
	private $__mail;
	public function mail()
	{
		// TODO: Finalise implementation of mail modules.
	}
	
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class FatModule
{
	// Finalise implementation of module abstract class
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class FatException extends Exception
{
	// Do nothing!
}
