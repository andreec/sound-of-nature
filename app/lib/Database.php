<?php

/**
 *  Class for handle database connection and I/O
 */
class Database {

    /**
     *  Constructor
     *  @params
     *      $config: config file 'app/config.inc.php'
     */
    public function __construct($config) {
		$this->__config = $config;
	}

    /**
	 * Application Config
	 */
	private $__config;
	public function config($key)
	{
		if(isset($this->__config[$key])) {
			return $this->__config[$key];
		}
		return null;
	}

    /**
     *  Create PDO instance
	 */
	private $__pdo;
	public function pdo()
	{
		if(!isset($this->__pdo)) {
			$this->__pdo = new PDO(
				"mysql:host=" . $this->config('db.host')/* . ";port:" . $this->config('db.port')*/ . ";dbname=" .  $this->config('db.name') . ";",
				$this->config('db.user'), $this->config('db.pass')
			);
		}
		
		return $this->__pdo;
	}
	
	/**
	 *  Create NotORM instance
	 *  @return notorm
	 */
	private $__orm;
	public function orm()
	{
		if(!isset($this->__orm)) {
			require_once $this->config('path.lib') . '/NotORM.php';
			$this->__orm = new NotORM($this->pdo(), new NotORM_Structure_Convention(
				'id', // Primary
				'%s_id', // Foreign
				'%s', // Table
				$this->config('db.prefix') // Prefix
			));
		}
		return $this->__orm;
	}
}

?>
