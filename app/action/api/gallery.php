<?php

session_start();

$order = isset($_GET['order']) && $_GET['order'] == 'ascending' ? 'ASC' : 'DESC';
$category = isset($_GET['category']) ? $_GET['category'] : '';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';

$Submission = new Submission();
$getSubmission = $Submission->getAll();

$totalSubmission = count($getSubmission);

$limit = 5;
$adjecents = 2;

$totalPage = ceil($totalSubmission/$limit);

$totalSubmission > $limit ? $isPagination = true : $isPagination = false;

if (!empty($_GET['page']) || isset($_GET['page'])) {
    $start = ($_GET['page'] - 1) * $limit;
    $page = $_GET['page'];
} else {
    $start = 0;
    $page = 1;
}

if (!empty($category) && $category == 'title') {
	$limitSubmission = $Submission->getAll($limit,$start,$order,null,null,$keyword);
} else if (!empty($category) && $category == 'listened') {
	$limitSubmission = $Submission->getAll($limit,$start,null,$order,null,$keyword);
} else if (!empty($category) && $category == 'composer') {
	$composer = $_GET['composer'];
	$limitSubmission = $Submission->getAll($limit,$start,null,null,$order,$keyword);
} else {
	$limitSubmission = $Submission->getAll($limit,$start,null,null,null,$keyword);
}

$totalPage = ceil($limitSubmission['total'] / $limit);

if ($page <= $adjecents) {
    $start = ($page - $adjecents);
    $start = $start < 0 ? 1 : $page - 1;
    
    $stop = $totalPage - ($adjecents * 2) + 1;
    $stop = $stop < 0 ? $totalPage : $stop;
} else if ($page > $adjecents && $page <= ($totalPage - $adjecents)) {
    $start = $page - $adjecents;
    
    $stop = $page + $adjecents;
} else if ($page >= ($totalpage - $adjecents)) {
    $start = $totalPage - ($limit - 1); 
    
    $stop = $totalPage;
}

if ($page > $stop)
    $stop = $totalPage;
    
if ($stop - $start > $limit)
    $stop = $limit;
    
$return_data = [];
foreach ($limitSubmission as $row) {
    if (isset($row['id']) && !empty($row['id'])) {
        $return_data['gallery'][] = array(
            'photo' => $row['userdata']['fb_id'],
            'title' => $row['title'],
            'name' => $row['userdata']['name'],
            'listen' => $row['total_listener'],
            'record' => $row["record_data"],
            'submission_id' => $row['id']
        );
    }
}

$return_data['pagination']['active'] = isset($_GET['page']) ? $_GET['page'] : 1;
$return_data['pagination']['start'] = $start;
$return_data['pagination']['stop'] = $stop;

return (object) array(
    'type' => 'JSON',
    'vars' => array(
	    $return_data
    )
);
