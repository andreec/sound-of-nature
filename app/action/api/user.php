<?php
session_start();

switch ($params[0]) {
	case 'connect':
		$data = array(
			'name' => $_POST['name'],
			'email' => $_POST['email'],
			'fb_id' => $_POST['fb_id'],
		);
		$user = new User();
		$insertUser = $user->register($data);
		
		$user_retrieve = $user->retrieve($insertUser['data']['id']);
		if ($insertUser['status'] == 'register') {
			$_SESSION['userdata']['user_id'] = $user_retrieve['id'];
			$_SESSION['userdata']['name'] = $user_retrieve['name'];
			$return_data = array(
	            'status' => 'succeed',
		        'data' => array(
		        	'url' => 'register',
		        )
	        );
		} else {
			$_SESSION['userdata']['user_id'] = $user_retrieve['id'];
			$_SESSION['userdata']['name'] = $user_retrieve['name'];
			$return_data = array(
	            'status' => 'failed',
		        'data' => array(
		        	'url' => 'like',
		        )
	        );
		}
		break;
	case 'register':
		$data = array(
			'name' => $_POST['name'],
			'email' => $_POST['email'],
		);
		$userID = $_SESSION['userdata']['user_id'];
		$user = new User();
		$updateUser = $user->updateUserdata($userID,$data);
		if ($updateUser || (isset($_POST['name']) && isset($_POST['email']))) {
			$status = 'succeed';
		} else {
			$status = 'failed';
		}
		$return_data = array(
            'status' => $status
        );
		break;
	case 'submit-music':
		$data = array(
			'user_id' => $_SESSION['userdata']['user_id'],
			'title' => $_POST['title'],
			'record_data' => $_POST['recordData'],
		);
		$submission = new Submission();
		$insertSubmission = $submission->insert($data);
		if($insertSubmission) {
			$return_data = array(
				'status' => 'succeed',
				'page' => 'thanks',
				'submission_id' => $insertSubmission['id']
			);
			//$this->redirect('/thanks/'.$insertSubmission['id']);
		} else {
			$return_data = array(
				'user_id' => $_SESSION['userdata']['user_id'],
				'status' => 'failed',
				'page' => 'playground'
			);
			//$this->redirect('/playground');
		}
		break;
	case 'counter':
		$data = array(
			'id' => $_POST['submission_id'],
			'user_id' => $_SESSION['userdata']['user_id'],
			'user_submission_id' => $_POST['submission_id']
		);
		$submission = new Submission();
		$addCounterSubmission = $submission->counter($data);
		if ($addCounterSubmission != false) {
			$return_data = array(
	            'status' => 'success',
		        'data' => array(
		        	'listened' => $addCounterSubmission,
		        ),
	        );
		} else {
			$return_data = array(
	            'status' => 'failed',
		        'data' => array(),
	        );
		}
		break;
	case 'retrieve': {
	    $user = new User();
	    $user_retrieve = $user->retrieve($_POST['id']);
	    
	    if ($user_retrieve) {
	        $return_data = array(
	            'status' => 'succeed',
		        'data' => array(
		            'name' => $user_retrieve['name'],
		            'email' => $user_retrieve['email']
		        )
	        );
	        
	        break;
	    }
	    
        $return_data = array(
            'status' => 'failed',
            'data' => array(),
        );
        
        break;
	}
	default:
		echo 'API Not Provided Yet';exit();
		break;
}

return (object) array(
    'type' => 'JSON',
    'vars' => array(
	    $return_data
    )
);

?>
