<?php

session_start();

if (!isset($_SESSION['userdata']['name']) && 
    empty($_SESSION['userdata']['name'])) {

	return (object) array(
		'views' => array(
			'/page/connect',
		),
		'vars' => array(
			'body_id' => 'home',
			'class' => 'no-login',
			'backgroundtype' => 'park'
		)
	);

}
    
$user = new User();

$user_data = $user->retrieve($_SESSION['userdata']['user_id']);

return (object) array(
	'views' => array(
		'/page/register',
	),
	'vars' => array(
		'body_id' => 'register',
		'name' => $user_data['name'],
		'email' => $user_data['email'],
		'backgroundtype' => 'park',
	)
);
