<?php

session_start();

if (!isset($_SESSION['userdata']['name']) && 
    empty($_SESSION['userdata']['name'])) {

	return (object) array(
		'views' => array(
			'/page/connect',
		),
		'vars' => array(
			'body_id' => 'home',
			'class' => 'no-login',
			'backgroundtype' => 'park'
		)
	);

}

$name = $_SESSION['userdata']['name'];

return (object) array(
	'views' => array(
		'/page/playground'
	),
	'vars' => array(
		'body_id' => 'playground',
		'name' => $name,
		'backgroundtype' => 'waterfall',
	)
);
