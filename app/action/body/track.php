<?php

session_start();

if (!isset($_SESSION['userdata']['name']) && 
    empty($_SESSION['userdata']['name'])) {

    return (object) array(
        'views' => array(
            '/page/connect',
        ),
        'vars' => array(
            'body_id' => 'home',
            'class' => 'no-login',
            'backgroundtype' => 'park'
        )
    );

}
    

$submissionID = $_REQUEST['id'];

$submission = new Submission();
$detailSubmission = $submission->getDetail($submissionID);

//print_r($detailSubmission);exit;

$name = $_SESSION['userdata']['name'];

return (object) array(
	'views' => array(
		'/page/track'
	),
	'vars' => array(
		'body_id' => 'track',
		'submission' => $detailSubmission,
		'name' => $name,
		'id' => $submissionID,
		'backgroundtype' => 'waterfall',
	)
);
