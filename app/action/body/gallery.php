<?php

session_start();

if (!isset($_SESSION['userdata']['name']) && 
    empty($_SESSION['userdata']['name'])) {

    return (object) array(
        'views' => array(
            '/page/connect',
        ),
        'vars' => array(
            'body_id' => 'home',
            'class' => 'no-login',
            'backgroundtype' => 'park'
        )
    );

}

$Submission = new Submission();
$getSubmission = $Submission->getAll();

$totalSubmission = count($getSubmission);

if (isset($_GET['title']))
	$search['title'] = $_GET['title'];

if (isset($_GET['composer']))
	$search['composer'] = $_GET['composer'];

if (isset($_GET['listened']))
	$search['listened'] = $_GET['listened'];

$limit = 5;
$adjecents = 2;

$totalPage = ceil($totalSubmission/$limit);

$totalSubmission > $limit ? $isPagination = true : $isPagination = false;

if (!empty($_GET['page']) || isset($_GET['page'])) {
    $start = ($_GET['page'] - 1) * $limit;
    $page = $_GET['page'];
} else {
    $start = 0;
    $page = 1;
}

if (isset($_GET['title'])) {
	$title = $_GET['title'];
	$limitSubmission = $Submission->getAll($limit,$start,$title,null,null,null);
} else if (isset($_GET['listened'])) {
	$listened = $_GET['listened'];
	$limitSubmission = $Submission->getAll($limit,$start,null,$listened,null,null);
} else if (isset($_GET['composer'])) {
	$composer = $_GET['composer'];
	$limitSubmission = $Submission->getAll($limit,$start,null,null,$composer,null);
} else if (isset($_GET['search'])) {
	$search = $_GET['search'];
	$limitSubmission = $Submission->getAll($limit,$start,null,null,null,$search);
} else {
	$limitSubmission = $Submission->getAll($limit,$start,null,null,null,null);
}

$totalPage = ceil($limitSubmission['total'] / $limit);

if ($page <= $adjecents) {
    $start = ($page - $adjecents);
    $start = $start < 0 ? 1 : $page - 1;
    
    $stop = $totalPage - ($adjecents * 2) + 1;
    $stop = $stop < 0 ? $totalPage : $stop;
} else if ($page > $adjecents && $page <= ($totalPage - $adjecents)) {
    $start = $page - $adjecents;
    
    $stop = $page + $adjecents;
} else if ($page >= ($totalpage - $adjecents)) {
    $start = $totalPage - ($limit - 1); 
    
    $stop = $totalPage;
}

if ($page > $stop)
    $stop = $totalPage;
    
if ($stop - $start > $limit)
    $stop = $limit;
    
return (object) array(
	'views' => array(
		'/page/gallery'
	),
	'vars' => array(
		'body_id' => 'gallery',
		'submission' => $limitSubmission,
        'page' => $page,
        'totalPage' => $totalPage,
        'start' => $start,
        'stop'  => $stop,
        'isPagination' => $isPagination,
        'title' => $title,
        'listened' => $listened,
        'composer' => $composer,
        'search' => $search,
        'backgroundtype' => 'waterfall'
	)
);
