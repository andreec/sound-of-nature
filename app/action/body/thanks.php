<?php

session_start();

if (!isset($_SESSION['userdata']['name']) && 
    empty($_SESSION['userdata']['name'])) {

	return (object) array(
		'views' => array(
			'/page/connect',
		),
		'vars' => array(
			'body_id' => 'home',
			'class' => 'no-login',
			'backgroundtype' => 'park'
		)
	);

}
    
$db = $this->orm();

$submission = $db->user_submission[$_REQUEST['id']];

return (object) array(
	'views' => array(
		'/page/thanks'
	),
	'vars' => array(
		'body_id' => 'thanks',
		'submission_id' => $submission['id'],
		'title' => $submission['title'],
		'backgroundtype' => 'waterfall',
	)
);
