<?php
session_start();

if (!isset($_SESSION['userdata']['name']) && 
    empty($_SESSION['userdata']['name'])) {

	return (object) array(
		'views' => array(
			'/page/connect',
		),
		'vars' => array(
			'body_id' => 'home',
			'class' => 'no-login',
			'backgroundtype' => 'park'
		)
	);

}    
    
$Submission = new Submission();
$getSubmission = $Submission->getTopSong();

$name = $_SESSION['userdata']['name'];

return (object) array(
	'views' => array(
		'/page/like'
	),
	'vars' => array(
		'body_id' => 'like',
		'submission' => $getSubmission,
		'name' => $name,
		'backgroundtype' => 'park'
	)
);
