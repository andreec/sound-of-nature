<?php

session_start();
if (!isset($_SESSION['admin_user_session']) &&
    !$_SESSION['admin_user_session']) {
    
    $this->redirect('cms/login');
}

if (isset($params)) {
    $page = $params[0];
}

switch($page) {
    case 'form': {
        $body = '/cms/user/member/form';
        $head = '/cms/user/member/form-head';
        break;
    }
    default: {
        $body = '/cms/user/member/list';
        $head = '/cms/user/member/list-head';
        break;
    }
}

return (object) array(
	'views' => array(
		'/cms/layout',
		array(
			'body' => $body,
			'header' => '/cms/header',
			'sidebar' => '/cms/menu-sidebar',
			'head' => $head
		)
	)
);
