<?php

session_start();
if (!isset($_SESSION['admin_user_session']) &&
    !$_SESSION['admin_user_session']) {
    
    $this->redirect('cms/login');
}

if (isset($params)) {
    $page = $params[0];
}

switch($page) {
    case 'form': {
        $body = '/cms/user/listener/form';
        $head = '/cms/user/listener/form-head';
        break;
    }
    default: {
        $body = '/cms/user/listener/list';
        $head = '/cms/user/listener/list-head';
        break;
    }
}

return (object) array(
	'views' => array(
		'/cms/layout',
		array(
			'body' => $body,
			'header' => '/cms/header',
			'sidebar' => '/cms/menu-sidebar',
			'head' => $head
		)
	)
);
