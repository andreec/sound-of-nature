<?php

session_start();
if (!isset($_SESSION['admin_user_session']) &&
    !$_SESSION['admin_user_session']) {
    
    $this->redirect('cms/login');
}

if (isset($params)) {
    $page = $params[0];
}

switch($page) {
    case 'form': {
        $body = '/cms/user/submission/form';
        $head = '/cms/user/submission/form-head';
        break;
    }
    default: {
        $body = '/cms/user/submission/list';
        $head = '/cms/user/submission/list-head';
        break;
    }
}

return (object) array(
	'views' => array(
		'/cms/layout',
		array(
			'body' => $body,
			'header' => '/cms/header',
			'sidebar' => '/cms/menu-sidebar',
			'head' => $head
		)
	)
);
