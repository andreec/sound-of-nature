<?php

session_start();
if (!isset($_SESSION['admin_user_session']) &&
    !$_SESSION['admin_user_session']) {
    
    $this->redirect('cms/login');
}

return (object) array(
	'views' => array(
		'/cms/layout',
		array(
			'body' => '/cms/dashboard',
			'header' => '/cms/header',
			'sidebar' => '/cms/menu-sidebar',
			'head' => '/cms/layout-head'
		)
	)
);
