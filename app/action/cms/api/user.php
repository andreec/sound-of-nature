<?php

$db = $this->orm();

$api = $params[0];

switch($api) {
    case 'pagination': {
        $offset = $_GET['start'];
        $limit = $_GET['length'];
        $draw = $_GET['draw'];

        $search_value = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';

        $total_data = $db->user()->count();

        // Check for search or default list
        if (empty($search_value)) {
            $user_list = $db->user();
        } else {
            $user_list = $db->user()
                ->where('name LIKE ?', "%$search_value%")
                ->or('email LIKE ?', "%$search_value%")
                ->or('fb_id LIKE ?', "%$search_value%")
                ->or('registered LIKE ?', "%$search_value%")
                ->or('is_active LIKE ?', "%$search_value%");
        }

        // Add order by
        if (isset($_GET['order']) && count($_GET['order'])) {
            $order = ($_GET['order'][0]['dir'] === 'asc') ? 'ASC' : 'DESC';
            
            switch($_GET['order'][0]['column']) {
                case '0': {
                    $user_list = $user_list->order("id $order");
                    break;
                }
                case '1': {
                    $user_list = $user_list->order("email $order");
                    break;
                }
                case '2': {
                    $user_list = $user_list->order("fb_id $order");
                    break;
                }
                case '3': {
                    $user_list = $user_list->order("registered $order");
                    break;
                }
                case '4': {
                    $user_list = $user_list->order("is_active $order");
                    break;
                }
                default: {
                    $user_list = $user_list->order("id $order");
                    break;
                }
            }
        }
        
        $total_filtered = count($user_list);
        $user_list = $user_list->limit($limit, $offset);

        $user_data = array();
        foreach ($user_list as $user) {
            $user_data[] = array(
                'name' => $user['name'],
                'email' => $user['email'],
                'fb_id' => $user['fb_id'],
                'registered' => $user['registered'],
                'is_active' => $user['is_active'],
                'id' => $user['id']
            );
        }

        // Data structure for datatables
        $data = array(
            'draw'            => $_GET['draw'],
            'recordsTotal'    => $total_data,
            'recordsFiltered' => $total_filtered,
            'data'            => $user_data
        );
        break;
    }
    case 'update': {
        $user = $db->user[$_POST['id']];
        
        $is_active = ($_POST['activeRadio'] == 'active') ? true : false;
        $row = $user->update(array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'is_active' => $is_active
        ));
        
        if($row) {
            $data = array(
                'status' => 'succeed'
            );
        } else {
            $data = array(
                'status' => 'failed',
                'message' => 'Cannot update user data!'
            );
        }
        break;
    }
    default: {
        break;
    }
}
	    
return (object) array(
    'type' => 'JSON',
    'vars' => array(
        $data
    )
);
