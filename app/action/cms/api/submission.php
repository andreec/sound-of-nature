<?php

$db = $this->orm();

$api = $params[0];

switch($api) {
    case 'pagination': {
        $offset = $_GET['start'];
        $limit = $_GET['length'];
        $draw = $_GET['draw'];

        $search_value = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';

        $total_data = $db->user_submission()->count();

        // Check for search or default list
        if (empty($search_value)) {
            $submission_list = $db->user_submission();
        } else {
            $submission_list = $db->user_submission()
                ->or('user_id', $db->user()
                    ->select('id')
                    ->where('name LIKE ?', "%$search_value%"))
                ->or('title LIKE ?', "%$search_value%")
                ->or('total_listener LIKE ?', "%$search_value%")
                ->or('created LIKE ?', "%$search_value%")
                ->or('is_active LIKE ?', "%$search_value%");
        }

        // Add order by
        if (isset($_GET['order']) && count($_GET['order'])) {
            $order = ($_GET['order'][0]['dir'] === 'asc') ? 'ASC' : 'DESC';
            
            switch($_GET['order'][0]['column']) {
                case '0': {
                    $submission_list = $submission_list->order("user_id $order");
                    break;
                }
                case '1': {
                    $submission_list = $submission_list->order("title $order");
                    break;
                }
                case '2': {
                    $submission_list = $submission_list->order("total_listener $order");
                    break;
                }
                case '3': {
                    $submission_list = $submission_list->order("created $order");
                    break;
                }
                case '4': {
                    $submission_list = $submission_list->order("is_active $order");
                    break;
                }
                default: {
                    $submission_list = $submission_list->order("id $order");
                    break;
                }
            }
        }
        
        $total_filtered = count($submission_list);
        $submission_list = $submission_list->limit($limit, $offset);

        $submission_data = array();
        foreach ($submission_list as $submission) {
            $user = $db->user[$submission['user_id']];
            $submission_data[] = array(
                'name' => $user['name'],
                'title' => $submission['title'],
                'total_listener' => $submission['total_listener'],
                'created' => $submission['created'],
                'is_active' => $submission['is_active'],
                'id' => $submission['id']
            );
        }

        // Data structure for datatables
        $data = array(
            'draw'            => $_GET['draw'],
            'recordsTotal'    => $total_data,
            'recordsFiltered' => $total_filtered,
            'data'            => $submission_data
        );
        break;
    }
    case 'update': {
        $user = $db->user[$_POST['id']];
        
        $is_active = ($_POST['activeRadio'] == 'active') ? true : false;
        $row = $user->update(array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'is_active' => $is_active
        ));
        
        if($row) {
            $data = array(
                'status' => 'succeed'
            );
        } else {
            $data = array(
                'status' => 'failed',
                'message' => 'Cannot update user data!'
            );
        }
        break;
    }
    case 'delete': {
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            $data = array(
                'status' => 'failed',
                'message' => 'Error on input!'
            );
            
            break;
        }
        $user_submission = $db->user_submission[$_POST['id']];
        
        if (!$user_submission) {
            $data = array(
                'status' => 'failed',
                'message' => 'Data not exist!'
            );
            
            break;
        }
        
        $user_submission->delete();
        $data = array('status' => 'succeed');
        break;
    }
    default: {
        break;
    }
}
	    
return (object) array(
    'type' => 'JSON',
    'vars' => array(
        $data
    )
);
