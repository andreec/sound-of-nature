<?php

$db = $this->orm();

$api = $params[0];

switch($api) {
    case 'pagination': {
        $offset = $_GET['start'];
        $limit = $_GET['length'];
        $draw = $_GET['draw'];

        $search_value = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';

        $total_data = $db->user_listener()->count();

        // Check for search or default list
        if (empty($search_value)) {
            $listener_list = $db->user_listener();
        } else {
            $listener_list = $db->user_listener()
                ->or('user_id', $db->user()
                    ->select('id')
                    ->where('name LIKE ?', "%$search_value%"))
                ->or('user_submission_id', $db->user_submission()
                    ->where('title LIKE ?', "%$search_value%"))
                ->or('created LIKE ?', "%$search_value%");
        }

        // Add order by
        if (isset($_GET['order']) && count($_GET['order'])) {
            $order = ($_GET['order'][0]['dir'] === 'asc') ? 'ASC' : 'DESC';
            
            switch($_GET['order'][0]['column']) {
                case '0': {
                    $listener_list = $listener_list->order("user_submission_id $order");
                    break;
                }
                case '1': {
                    $listener_list = $listener_list->order("user_id $order");
                    break;
                }
                case '2': {
                    $listener_list = $listener_list->order("created $order");
                    break;
                }
                default: {
                    $listener_list = $listener_list->order("id $order");
                    break;
                }
            }
        }
        
        $total_filtered = count($listener_list);
        $listener_list = $listener_list->limit($limit, $offset);

        $listener_data = array();
        foreach ($listener_list as $listener) {
            $user = $db->user[$listener['user_id']];
            $submission = $db->user_submission[$listener['user_submission_id']];
            
            $listener_data[] = array(
                'title' => $submission['title'],
                'listener' => $user['name'],
                'created' => $listener['created'],
                'id' => $listener['id']
            );
        }

        // Data structure for datatables
        $data = array(
            'draw'            => $_GET['draw'],
            'recordsTotal'    => $total_data,
            'recordsFiltered' => $total_filtered,
            'data'            => $listener_data
        );
        break;
    }
    case 'update': {
#        $user = $db->user[$_POST['id']];
#        
#        $is_active = ($_POST['activeRadio'] == 'active') ? true : false;
#        $row = $user->update(array(
#            'name' => $_POST['name'],
#            'email' => $_POST['email'],
#            'is_active' => $is_active
#        ));
#        
#        if($row) {
#            $data = array(
#                'status' => 'succeed'
#            );
#        } else {
#            $data = array(
#                'status' => 'failed',
#                'message' => 'Cannot update user data!'
#            );
#        }
        break;
    }
    default: {
        break;
    }
}
	    
return (object) array(
    'type' => 'JSON',
    'vars' => array(
        $data
    )
);
