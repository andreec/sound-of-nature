<?php

$file = null;
$db = $this->orm();

if (isset($params)) {
    switch($params[0]) {
        case 'user-member': {
            $file = '/cms/user/member/form';
            break;
        }
        default: {
            // TODO: return error
            break;
        }
    }
} else {
    // TODO: return error
}

$user = $db->user[$_GET['id']];
$user_data = array(
    'id' => $user['id'],
    'name' => $user['name'],
    'email' => $user['email'],
    'fb_id' => $user['fb_id'],
    'registered' => date('d M Y H:i:s', strtotime($user['registered'])),
    'is_active' => $user['is_active']
);

$content = $this->getView($file, $user_data);

$data = array(
    'content' => $content
);

return (object) array(
    'type' => 'JSON',
    'vars' => array(
        $data
    )
);
