<?php

session_start();

return (object) array(
	'views' => array(
		'/layout',
		array(
			'header' => '/_header',
			'footer' => '/_footer'
		)
	)
);