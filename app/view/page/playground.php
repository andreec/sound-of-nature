<div id="contentdata" data-body_id="<?php echo (isset($body_id)) ? $body_id : ''; ?>" data-class="<?php echo (isset($class)) ? $class : ''; ?>" data-backgroundtype="<?php echo (isset($backgroundtype)) ? $backgroundtype : ''; ?>" data-user="<?php echo (isset($name)) ? $name : ''; ?>"></div>
<div id="itemholder">
		
</div>

<div class="tutorial">
	<div class="how">
		how to play
	</div>
	<div class="wrapper-tutorial">
		<span class="tutorialBG"></span>
		<h1>how to play</h1>
		<div>
			<div class="numeric"></div>
			<div class="tutor"></div>
			Choose the variant
		</div>
		<div>
			<div class="numeric"></div>
			<div class="tutor"></div>
			Drop it to the stage
		</div>
		<div>
			<div class="numeric"></div>
			<div class="tutor"></div>
			listen
		</div>
		<div>
			<div class="numeric"></div>
			<div class="tutor"></div>
			Combine with other variants
		</div>
		<div>
			<div class="numeric"></div>
			<div class="tutor"></div>
			Press The Record Button and Start Mixing!
		</div>
		<div>
			<div class="numeric"></div>
			<div class="tutor"></div>
			Save your track and share
		</div>
		<div class="last-copy">you can't leave this page while you're mixing.<br>kreasimu akan otomatis terhenti saat kamu keluar dari halaman ini.</div>
		<div class="close js-close-tutorial">TUTUP</div>
	</div>
</div>

<!-- <div class="wrapper" style="display:none;"> -->
<div class="wrapper">
	<div class="hanging-board">
		<div id="recordingboard">
			<div class="rec-wrapper">
				<div class="beat"></div>
				<div class="btn">					
					<a class="record js-action-record"></a>
					<span>Record</span>	
				</div>
				<div class="play-wrapper">
					<div class="back"></div>
					<div class="front"></div>
				</div>
				<div class="time-wrapper">
					0 s
				</div>
			</div>
		</div>
		<div id="savingboard">
			<form id="saveInput" action="api/user/submit-music" method="post">
				<input type="hidden" name="recordData">
				<div class="saveInput">
					<input type="text" name="title" value="" placeholder="Masukkan judul track kamu di sini" maxlength="30"></input>
				</div>
			</form>
			<a class="btn standing down left js-action-cancelrecord" style="top:153px;">
				CANCEL
			</a>
			<a class="btn standing down right js-action-saverecord" style="top:153px;">
				SAVE
			</a>
			<div class="rec-wrapper">
				<div class="btn">
					<a class="play js-action-play" data-progressbar="#savingboard .play-wrapper .front" data-secondview="#savingboard .time-wrapper" data-recording="lastrecording"></a>
				</div>
				<div class="play-wrapper">
					<div class="back"></div>
					<div class="front"></div>
				</div>
				<div class="time-wrapper">
					0 s
				</div>
			</div>
		</div>
	</div>

	<div class="countShow">			
		<div class="count1"></div>
		<div class="count2"></div>
		<div class="count3"></div>
		<div class="count4"></div>			
	</div>
	<div class="product" id="dropzone">
		<div class="dropzone dz1" data-type="bass">
			<div class="music"></div>
			<div class="glow"></div>
			<div class="splash">
				<div class="splash1"></div>
				<div class="splash2"></div>
				<div class="splash3"></div>
				<div class="splash4"></div>
				<div class="splash5"></div>
				<div class="splash6"></div>
				<div class="splash7"></div>
			</div>
		</div>
		<div class="dropzone dz2" data-type="drum">
			<div class="music"></div>
			<div class="glow"></div>
			<div class="splash">
				<div class="splash1"></div>
				<div class="splash2"></div>
				<div class="splash3"></div>
				<div class="splash4"></div>
				<div class="splash5"></div>
				<div class="splash6"></div>
				<div class="splash7"></div>
			</div>
		</div>
		<div class="dropzone dz3" data-type="effect">
			<div class="music"></div>
			<div class="glow"></div>
			<div class="splash">
				<div class="splash1"></div>
				<div class="splash2"></div>
				<div class="splash3"></div>
				<div class="splash4"></div>
				<div class="splash5"></div>
				<div class="splash6"></div>
				<div class="splash7"></div>
			</div>
		</div>
		<div class="dropzone dz4" data-type="pad">
			<div class="music"></div>
			<div class="glow"></div>
			<div class="splash">
				<div class="splash1"></div>
				<div class="splash2"></div>
				<div class="splash3"></div>
				<div class="splash4"></div>
				<div class="splash5"></div>
				<div class="splash6"></div>
				<div class="splash7"></div>
			</div>
		</div>
		<div class="dropzone dz5" data-type="synth">
			<div class="music"></div>
			<div class="glow"></div>
			<div class="splash">
				<div class="splash1"></div>
				<div class="splash2"></div>
				<div class="splash3"></div>
				<div class="splash4"></div>
				<div class="splash5"></div>
				<div class="splash6"></div>
				<div class="splash7"></div>
			</div>
		</div>
	</div>
	<div class="bottom-things">
		<span class="lemon_f"></span>
		<span class="vanilla_f"></span>
		<span class="raspberry_f"></span>
		<div class="slider">
			<a class="btn standing up left prev"></a>
			<a class="btn standing up right next"></a>
			<div class="list">
				<div class="slide-product" id="slider">
					<div class="item british" data-type="british"></div>
					<div class="item cranberry" data-type="cranberry"></div>
					<div class="item lemon" data-type="lemon"></div>
					<div class="item lime" data-type="lime"></div>
					<div class="item mint" data-type="mint"></div>
					<div class="item raspberry" data-type="raspberry"></div>
					<div class="item spearmint" data-type="spearmint"></div>
					<div class="item blackmint" data-type="blackmint"></div>						
					<div class="item lemonlime" data-type="lemonlime"></div>						
					<div class="item walnut" data-type="walnut"></div>
					<div class="item butter" data-type="butter"></div>						
				</div>
			</div>
		</div>
	</div>
	
</div>