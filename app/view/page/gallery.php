<div id="contentdata" data-body_id="<?php echo (isset($body_id)) ? $body_id : ''; ?>" data-class="<?php echo (isset($class)) ? $class : ''; ?>" data-backgroundtype="<?php echo (isset($backgroundtype)) ? $backgroundtype : ''; ?>"></div>
<div class="hanging-board">
	<a href="#home" class="btn hanging gal in-gallery">Home</a>
	<div class="board-extend"></div>
	<div class="gallery-box">
		<div class="gallery-header">
			<h2>Nature's Masterpiece</h2>
			<p class="des">Cari Kreasi Favoritmu!</p>
		</div>

		<div class="gallery-content clearfix">
			<div class="gallery-dropdown clearfix">
				<?php if (isset($title) or !empty($title)) { ?>
					<?php if($title == 'desc') { ?>
					<a class="new-title" href="<?php echo "gallery?title=asc";?>" data-title="title">
					<div class="select-child select-title">
						<span class="arrow">
							<span class="child-arrow arrow-bottom"></span>
						</span>
					<?php } else { ?>
					<a class="new-title" href="<?php echo "gallery?title=desc";?>" data-title="title">
					<div class="select-child select-title">
						<span class="arrow">
							<span class="child-arrow arrow-top"></span>
						</span>
					<?php } ?>
				<?php } else { ?>
					<a class="new-title" href="<?php echo "gallery?title=desc";?>" data-title="title">
						<div class="select-child select-title">
				<?php } ?>
						<span class="text">Title</span>
					</div>
				</a>

				<?php if (isset($composer) or !empty($composer)) { ?>
					<?php if($composer == 'desc') { ?>
					<a class="new-composer" href="<?php echo "gallery?composer=asc";?>" data-title="composer">
						<div class="select-child select-title">
						<span class="arrow">
							<span class="child-arrow arrow-bottom"></span>
						</span>
					<?php } else { ?>
					<a class="new-composer" href="<?php echo "gallery?composer=desc";?>" data-title="composer">
						<div class="select-child select-title">
						<span class="arrow">
							<span class="child-arrow arrow-top"></span>
						</span>
					<?php } ?>
				<?php } else { ?>
					<a class="new-composer" href="<?php echo "gallery?composer=desc";?>" data-title="composer">
						<div class="select-child select-title">								
				<?php } ?>
						<span class="text">composer</span>
					</div>
				</a>

				<?php if (isset($listened) or !empty($listened)) { ?>
					<?php if($listened == 'desc') { ?>
					<a class="new-listen" href="<?php echo "gallery?listened=asc";?>" data-title="listened">
						<div class="select-child select-listened">
						<span class="arrow">
							<span class="child-arrow arrow-bottom"></span>
						</span>
					<?php } else { ?>
					<a class="new-listen" href="<?php echo "gallery?listened=desc";?>" data-title="listened">
						<div class="select-child select-listened">
						<span class="arrow">
							<span class="child-arrow arrow-top"></span>
						</span>
					<?php } ?>
				<?php } else { ?>
					<a class="new-listen" href="<?php echo "gallery?listened=desc";?>" data-title="listened">
						<div class="select-child select-listened">
				<?php } ?>
						<span class="text">listened</span>
					</div>
				</a>
			</div>

			<div class="clearfix"></div>

			<?php					
			if(count($submission)>1) { ?>
			<ul class="gallery-list">
				<?php if (!empty($submission) || count($submission) > 1) { ?>						
				<?php foreach ($submission as $row) { if (count($row) > 1) { ?>
				<li class="list hover">
					<div class="gal-img">
						<img src="<?php echo 'http://graph.facebook.com/'.$row['userdata']['fb_id'].'/picture?type=square'?>" alt="<?php echo $row['userdata']['name']; ?>" />
					</div>
			
					<div class="gal-title">
						<?php echo substr($row['title'], 0, 40); ?>
					</div>
			
					<div class="gal-composer">
						<?php echo substr($row['userdata']['name'], 0, 25) ?>
					</div>
			
					<div class="gal-listen">
						<?php echo $row['total_listener']; ?>
					</div>
			
					<div class="gal-play"></div>
					<!-- <a href="<?php echo 'detail/'.$row['id']; ?>" class="play"></a> -->
					<a class="play" href="#track_<?php echo $row['id']; ?>"></a>
				</li>
				<?php }} ?>
				<div class="emptySearch">Belum ada track yang tersimpan saat ini. Get yours listed here!</div>
				<?php } ?>
			</ul>
			<?php } 
			else 
			{
			?>	
				<p class="emptylist">Track tidak ditemukan</p>					
			<?php
			}
			?>

			<div class="gallery-bottom">
				<span class="setTimer"></span>
				<?php if($totalPage>1) { ?>
				<p class="nav">
					<?php if ($page > 1) { ?>
					<a href="gallery?page=<?php echo $page-1;?>&search=<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>"> &lt; </a>
					<?php } ?>
					<?php for($pageNum = $start; $pageNum <= $stop; $pageNum++) { ?>
					<a href="
						gallery?page=<?php echo $pageNum;?>&search=<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>
							<?php if(isset($title)){echo '&title='.$title;} ?>
							<?php if(isset($listened)){echo '&listened='.$listened;} ?>
							<?php if(isset($composer)){echo '&composer='.$composer;} ?>
							<?php //if(isset($search)){echo '&search='.$search;} ?>
						" <?php if($pageNum==$page)echo'class="active"'?>><?php echo $pageNum;?></a>
					<?php } ?>
					<?php if ($page < $totalPage) { ?>
					<a href="gallery?page=<?php echo $page+1;?>&search=<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>"> &gt; </a>
					<?php } ?>
				</p>
				<?php } ?>

				<form action="gallery" class="gallery-form" method="get">
					<input type="text" name="search" value="<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>">
					<button type="submit"></button>
				</form>
			</div>

		</div>

	</div>
</div>