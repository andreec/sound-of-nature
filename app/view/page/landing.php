<div id="contentdata" data-body_id="<?php echo (isset($body_id)) ? $body_id : ''; ?>" data-class="<?php echo (isset($class)) ? $class : ''; ?>" data-backgroundtype="<?php echo (isset($backgroundtype)) ? $backgroundtype : ''; ?>"></div>
<div class="wrapper">	
	<div class="hanging-board drbeat home">
		<div class="headset js-show-syarat"></div>
	</div>	
	<div class="hanging-board landingpage">
		<a class="btn hanging gal" href="#gallery">Track List</a>
		<a class="btn hanging mix" href="#playground">Start Mixing</a>
		<div>
			<h2>HERE IT GO</h2>
			<p class="intro">Bring your intense natural creation out!</p>			
			<div class="sub"></div>
			<?php if(count($submission)!=0) {?>				
			<div class="top-three">
				<?php foreach ($submission as $row) { ?>
				<div class="hover">
					<div class="rounded">
						<img src="<?php echo 'http://graph.facebook.com/'.$row['userdata']['fb_id'].'/picture?type=square'?>" alt="<?php echo $row['userdata']['name']; ?>" />
					</div>					
					<p>
						<span class="song"><?php echo substr($row['title'], 0, 40); ?></span>
						<span class="name"><?php echo substr($row['userdata']['name'], 0, 25); ?></span>
					</p>						
					<a class="play" href="#track_<?php echo $row['id']; ?>"></a>
				</div>
				<?php } ?>
			</div>
			<?php } 
			else 
			{
			?>	
				<p class="emptylist">Jadilah yang pertama mengeksplor suara alam sesuai karakter botol yang kamu inginkan. Bisa jadi, kreasimu lah yang akan menjadi track yang paling sering didengar.</p>					
			<?php
			}
			?>
		</div>
	</div>
</div>