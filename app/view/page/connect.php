<div class="fb-overlay"></div>
<div id="contentdata" data-body_id="<?php echo (isset($body_id)) ? $body_id : ''; ?>" data-class="<?php echo (isset($class)) ? $class : ''; ?>" data-backgroundtype="<?php echo (isset($backgroundtype)) ? $backgroundtype : ''; ?>" data-user="<?php echo (isset($name)) ? $name : ''; ?>"></div>
<div class="wrapper">
	<div class="hanging-board drbeat"></div>
	<div class="hanging-board fbconnect">
		<div>
			<h2>Bring your intense<br>natural creation out!</h2>
			<div class="copy-connect"></div>
			<img class="sign-with-fb" src="assets/img/button/fb-btn.png" alt="Sign in with Facebook" onclick="login()" />			
		</div>
	</div>
</div>