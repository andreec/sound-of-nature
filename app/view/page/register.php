<div id="contentdata" data-body_id="<?php echo (isset($body_id)) ? $body_id : ''; ?>" data-class="<?php echo (isset($class)) ? $class : ''; ?>" data-backgroundtype="<?php echo (isset($backgroundtype)) ? $backgroundtype : ''; ?>" data-user="<?php echo (isset($name)) ? $name : ''; ?>"></div>
<form id="formRegister" method="post" action="api/user/register">
	<div class="hanging-board">
		<a class="btn hanging alt js-validateform-register">
			Submit					
		</a>
		<div>
			<h2>Register</h2>
			<div class="textinput">
				<input type="text" name="name" placeholder="Name" value="<?php echo isset($name) ? $name : ''; ?>" maxlength="25"/>
			</div>
			<div class="textinput">
				<input type="text" name="email" placeholder="Email" value="<?php echo isset($email) ? $email : ''; ?>" />
			</div>
			<!-- <label class="custom checkbox checked"> -->
			<label class="custom checkbox">
				<input type="checkbox" name="iagree" />
				<span>Setuju dengan <a class="js-show-syarat">Syarat dan Ketentuan</a> yang berlaku?</span>
			</label>
			<div class="message"></div>
		</div>
	</div>
</form>