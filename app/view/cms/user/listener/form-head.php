<!-- bootstrap framework -->
	<link href="cms-assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	
	<!-- custom icons -->
		<!-- font awesome icons -->
		<link href="cms-assets/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
		<!-- ionicons -->	
		<link href="cms-assets/icons/ionicons/css/ionicons.min.css" rel="stylesheet" media="screen">
		<!-- flags -->
		<link rel="stylesheet" href="cms-assets/icons/flags/flags.css">
			

<!-- page specific stylesheets -->

	<!-- select2 -->
	<link rel="stylesheet" href="cms-assets/lib/select2/select2.css">
	<!-- bootstrap-datepicker -->
	<link rel="stylesheet" href="cms-assets/lib/bootstrap-datepicker/css/datepicker3.css">
	<!-- bootstrap switches -->
	<link href="cms-assets/lib/bootstrap-switch/build/css/bootstrap3/bootstrap-switch.css" rel="stylesheet">
	<!-- 2col multiselect -->
	<link href="cms-assets/lib/multi-select/css/multi-select.css" rel="stylesheet">
	<!-- date range picker -->
	<link rel="stylesheet" href="cms-assets/lib/bootstrap-daterangepicker/daterangepicker-bs3.css">
	<!-- magnific popup -->
	<link rel="stylesheet" href="cms-assets/lib/magnific-popup/magnific-popup.css">
			
	<!-- main stylesheet -->
	<link href="cms-assets/css/style.css" rel="stylesheet" media="screen">
	
	<!-- google webfonts -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
	
	<!-- moment.js (date library) -->
	<script src="cms-assets/lib/moment-js/moment.min.js"></script>
