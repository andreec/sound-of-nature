<!-- main content -->
<div id="main_wrapper">
	<div class="page_content">
	    <div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">User Submission</div>
						<div class="panel-body">
							<table id="dt-user-submission" class="table table-striped">
							    <thead>
									<tr>
										<th>Author</th>
										<th>Title</th>
										<th>Total Listener</th>
										<th>Created</th>
										<th>Status</th>
										<th></th>
										<th>ID</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>		
</div>

<!-- jQuery -->
	<script src="cms-assets/js/jquery.min.js"></script>
	<!-- easing -->
	<script src="cms-assets/js/jquery.easing.1.3.min.js"></script>
	<!-- bootstrap js plugins -->
	<script src="cms-assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- top dropdown navigation -->
	<script src="cms-assets/js/tinynav.js"></script>
	<!-- perfect scrollbar -->
	<script src="cms-assets/lib/perfect-scrollbar/min/perfect-scrollbar-0.4.8.with-mousewheel.min.js"></script>
	
	<!-- common functions -->
	<script src="cms-assets/js/tisa_common.js"></script>
	
	<!-- style switcher -->
	<script src="cms-assets/js/tisa_style_switcher.js"></script>
	
<!-- page specific plugins -->

	<!-- datatables -->
	<script src="cms-assets/lib/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="cms-assets/lib/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script src="cms-assets/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
	<script src="cms-assets/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
	
	<!-- AJAX datatables -->
	<script src="cms-assets/js/cms/ajax_datatables.js"></script>
	
	<!-- bootbox -->
	<script src="cms-assets/lib/bootbox/bootbox.js"></script>
	<!-- noty notifications -->
	<script src="cms-assets/lib/noty/js/noty/packaged/jquery.noty.packaged.js"></script>
	<!-- magnific popup -->
	<script src="cms-assets/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
	
	<!-- Popup -->
	<script src="cms-assets/js/cms/popup.js"></script>
