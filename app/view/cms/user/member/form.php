<!-- main content -->
<div>
	<div>
		<div class="row">
			<div class="col-md-8">
				<h1 class="page_title"><?php echo isset($name) ? $name : ''; ?></h1>
			</div>
		</div>
	</div>
	<div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="tabbable tabs-left">
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#user_tab_general" class="tab-default">General</a></li>
									<li><a data-toggle="tab" class="tab-default">Submission</a></li>
								</ul>
								<div class="tab-content">
									<div id="user_tab_general" class="tab-pane active">
									    <form id="form-member">
										    <div class="form-group">
											    <div class="row">
												    <div class="col-lg-6">
													    <label for="p_name" class="req">Name</label>
													    <input type="text" name="name" class="form-control" value="<?php echo isset($name) ? $name : ''; ?>">
													    <input type="hidden" name="id" value="<?php echo isset($id) ? $id : ''; ?>">
												    </div>
												    <div class="col-lg-6">
													    <label for="p_model">Email</label>
													    <input type="text" name="email" class="form-control" value="<?php echo isset($email) ? $email : ''; ?>">
												    </div>
											    </div>
										    </div>
										    <div class="form-group">
											    <div class="row">
												    <div class="col-lg-6">
													    <label for="p_name" class="req">FB ID</label>
													    <?php echo isset($fb_id) ? $fb_id : 'Not Connected to FB!'; ?>
												    </div>
												    <div class="col-lg-6">
													    <label for="p_model">Registered</label>
													    <?php echo isset($registered) ? $registered : ''; ?>
												    </div>
											    </div>
										    </div>
										    <div class="form-group">
										        <div class="row">
										            <div class="col-lg-4">
										                <label for="p_name" class="req">Active</label>
									                    <input type="radio" name="activeRadio" id="optionsRadiosActive" value="active" <?php echo $is_active ? 'checked' : ''; ?>>
													    Active
													    <input type="radio" name="activeRadio" id="optionsRadiosInactive" value="inactive" <?php echo !$is_active ? 'checked' : ''; ?>>
													    Inactive
							                        </div>
										        </div>
										    </div>
									    </form>
									</div>
									<div id="user_tab_submission" class="tab-pane">
										<div class="form-group">
											<div class="form-group">
												<div class="row">
													<div class="col-lg-3">
														<label for="p_sku">SKU</label>
														<input type="text" id="p_sku" class="form-control">
														<span class="help-block">Stock Keeping Unit</span>
													</div>
													<div class="col-lg-3">
														<label for="p_ean">EAN</label>
														<input type="text" id="p_ean" class="form-control">
														<span class="help-block">European Article Number</span>
													</div>
													<div class="col-lg-3">
														<label for="p_isbn">ISBN</label>
														<input type="text" id="p_isbn" class="form-control">
													</div>
													<div class="col-lg-3">
														<label for="p_mpn">MPN</label>
														<input type="text" id="p_mpn" class="form-control">
													</div>
												</div>
											</div>
											<div class="form-group form-sep">
												<div class="row">
													<div class="col-lg-3">
														<label for="p_date_available">Date Available</label>
														<input type="text" id="p_date_available" class="form-control">
													</div>
													<div class="col-lg-3">
														<label>Requires Shipping</label>
														<input class="bs_switch" type="checkbox" name="p_req_shipping" id="p_req_shipping" data-on-color="success" data-on-text="Yes" data-off-text="No">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- jQuery -->
	<script src="cms-assets/js/jquery.min.js"></script>
	<!-- easing -->
	<script src="cms-assets/js/jquery.easing.1.3.min.js"></script>
	<!-- bootstrap js plugins -->
	<script src="cms-assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- top dropdown navigation -->
	<script src="cms-assets/js/tinynav.js"></script>
	<!-- perfect scrollbar -->
	<script src="cms-assets/lib/perfect-scrollbar/min/perfect-scrollbar-0.4.8.with-mousewheel.min.js"></script>
	
	<!-- common functions -->
	<script src="cms-assets/js/tisa_common.js"></script>
	
	<!-- style switcher -->
	<script src="cms-assets/js/tisa_style_switcher.js"></script>
	
<!-- page specific plugins -->

	<!-- wysiwg editor -->
	<script src="cms-assets/lib/ckeditor/ckeditor.js"></script>
	<script src="cms-assets/lib/ckeditor/adapters/jquery.js"></script>
	<!-- select2 -->
	<script src="cms-assets/lib/select2/select2.min.js"></script>
	<!--  bootstrap-datepicker -->
	<script src="cms-assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!--  bootstrap switches -->
	<script src="cms-assets/lib/bootstrap-switch/build/js/bootstrap-switch.min.js"></script>
	<!--  2col multiselect -->
	<script src="cms-assets/lib/multi-select/js/jquery.multi-select.js"></script>
	<!-- multiupload -->
	<script src="cms-assets/lib/jQuery-UI/jquery.ui.widget.min.js"></script>
	<script src="cms-assets/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
	<script src="cms-assets/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
	<script src="cms-assets/lib/jQuery-File-Upload/js/extras/load-image.min.js"></script>
	<script src="cms-assets/lib/jQuery-File-Upload/js/jquery.fileupload-process.js"></script>
	<script src="cms-assets/lib/jQuery-File-Upload/js/jquery.fileupload-image.js"></script>
	<!-- date range picker -->
	<script src="cms-assets/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- magnific popup -->
	<script src="cms-assets/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
	
	<!-- ecommerce functions -->
	<script src="cms-assets/js/apps/tisa_ecommerce.js"></script>
	
	<!-- bootbox -->
	<script src="cms-assets/lib/bootbox/bootbox.js"></script>
	<!-- noty notifications -->
	<script src="cms-assets/lib/noty/js/noty/packaged/jquery.noty.packaged.js"></script>
	
	<!-- modals, lightbox functions -->
	<script src="cms-assets/js/apps/tisa_modals.js"></script>
