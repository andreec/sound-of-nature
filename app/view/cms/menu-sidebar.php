<!-- side navigation -->
<nav id="side_nav_acc_wrapper">
	<!--ul>
		<li>
			<a href="#">
			    <span class="ion-person"></span>
			    <span class="nav_title">User</span>
		    </a>
		    <div class="sub_panel"-->
		        <ul>
		            <li>
			            <a href="cms/user/member">
			                <span class="ion-person-stalker"></span>
			                <span class="nav_title">Member</span>
		                </a>
		            </li>
		            <li>
			            <a href="cms/user/submission">
			                <span class="ion-clipboard"></span>
			                <span class="nav_title">Submission</span>
		                </a>
		            </li>
		            <li>
			            <a href="cms/user/listener">
			                <span class="ion-chatbubble-working"></span>
			                <span class="nav_title">Listener</span>
		                </a>
		            </li>
		        </ul>
		    <!--/div>
		</li-->
		<!--li>
			<a href="#">
			    <span class="ion-music-note"></span>
			    <span class="nav_title">Sound Template</span>
		    </a>
		</li-->
	</ul>
</nav>
