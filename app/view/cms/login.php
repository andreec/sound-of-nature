<div class="login_container">
	<form id="login_form" action="cms/login" method="post">
		<h1 class="login_heading">Login <span>/ <a href="#" class="open_register_form">register</a></span></h1>
		<div class="form-group">
			<label for="login_username">Username</label>
			<input type="text" class="form-control input-lg" name="username" placeholder="User" id="login_username">
		</div>
		<div class="form-group">
			<label for="login_password">Password</label>
			<input type="password" class="form-control input-lg" name="password" placeholder="password" id="login_password">
		</div>
		<div class="submit_section">
			<button class="btn btn-lg btn-success btn-block">Continue</button>
		</div>
	</form>
</div>

<div class="modal fade" id="terms_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Terms & Conditions</h4>
			</div>
			<div class="modal-body">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus eaque tempora! Porro cumque labore voluptate dolore alias libero commodi deserunt unde aspernatur dignissimos quaerat similique maiores quasi eos optio quidem.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus eaque tempora! Porro cumque labore voluptate dolore alias libero commodi deserunt unde aspernatur dignissimos quaerat similique maiores quasi eos optio quidem.
			</div>
		</div>
	</div>
</div>

<!-- jQuery -->
<script src="cms-assets/js/jquery.min.js"></script>
<!-- bootstrap js plugins -->
<script src="cms-assets/bootstrap/js/bootstrap.min.js"></script>
<script>
	$(function() {
		// switch forms
		$('.open_register_form').click(function(e) {
			e.preventDefault();
			$('#login_form').removeClass().addClass('animated fadeOutDown');
			setTimeout(function() {
				$('#login_form').removeClass().hide();
				$('#register_form').show().addClass('animated fadeInUp');
			}, 700);
		})
		$('.open_login_form').click(function(e) {
			e.preventDefault();
			$('#register_form').removeClass().addClass('animated fadeOutDown');
			setTimeout(function() {
				$('#register_form').removeClass().hide();
				$('#login_form').show().addClass('animated fadeInUp');
			}, 700);
		})
	})
</script>
