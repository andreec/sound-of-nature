<!-- nvd3 charts -->
	<link rel="stylesheet" href="cms-assets/lib/novus-nvd3/nv.d3.min.css">
	<!-- owl carousel -->
	<link rel="stylesheet" href="cms-assets/lib/owl-carousel/owl.carousel.css">
			
	<!-- main stylesheet -->
	<link href="cms-assets/css/style.css" rel="stylesheet" media="screen">
	
	<!-- google webfonts -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
	
	<!-- moment.js (date library) -->
	<script src="cms-assets/lib/moment-js/moment.min.js"></script>
