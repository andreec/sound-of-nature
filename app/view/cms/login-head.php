<meta charset="UTF-8">
<title>Login Page</title>
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	
<!-- bootstrap framework -->
<link href="cms-assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- google webfonts -->
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
	
<link href="cms-assets/css/login.css" rel="stylesheet">
