<!-- top bar -->
<header class="navbar navbar-fixed-top" role="banner">
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="lang_menu">
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="user_profile.html"><span class="flag-FR"></span> France</a></li>
					<li><a href="mail_inbox.html"><span class="flag-IN"></span> India</a></li>
					<li><a href="tasks_summary.html"><span class="flag-BR"></span> Brasil</a></li>
					<li><a href="tasks_summary.html"><span class="flag-GB"></span> UK</a></li>
				</ul>
			</li>
			<li class="user_menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span class="navbar_el_icon ion-person"></span> <span class="caret"></span>
				</a>
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="cms/logout">Log Out</a></li>
				</ul>
			</li>
		</ul>
	</div>
</header>
