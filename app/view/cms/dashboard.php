<!-- main content -->
<div id="main_wrapper">
	<div class="page_content">
	    CMS Sound of Nature
	</div>		
</div>

<!-- jQuery -->
	<script src="cms-assets/js/jquery.min.js"></script>
	<!-- easing -->
	<script src="cms-assets/js/jquery.easing.1.3.min.js"></script>
	<!-- bootstrap js plugins -->
	<script src="cms-assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- top dropdown navigation -->
	<script src="cms-assets/js/tinynav.js"></script>
	<!-- perfect scrollbar -->
	<script src="cms-assets/lib/perfect-scrollbar/min/perfect-scrollbar-0.4.8.with-mousewheel.min.js"></script>
	
	<!-- common functions -->
	<script src="cms-assets/js/tisa_common.js"></script>
	
	<!-- style switcher -->
	<script src="cms-assets/js/tisa_style_switcher.js"></script>
	
<!-- page specific plugins -->
