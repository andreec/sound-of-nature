<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo $this->baseHref(); ?>">
		<meta charset="UTF-8">
		<title>CMS - Sound of Nature</title>
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
		
		<link rel="shortcut icon" href="favicon.ico" />
		
		<!-- bootstrap framework -->
		<link href="cms-assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		
		<!-- custom icons -->
			<!-- font awesome icons -->
			<link href="cms-assets/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
			<!-- ionicons -->	
			<link href="cms-assets/icons/ionicons/css/ionicons.min.css" rel="stylesheet" media="screen">
			<!-- flags -->
			<link rel="stylesheet" href="cms-assets/icons/flags/flags.css">
				
	
	<!-- page specific stylesheets -->

		<?php if(!empty($BODY['head'])) echo $BODY['head']; ?>
		
    </head>
    <body class="side_nav_accordion">
        
		<?php if(!empty($BODY['header'])) echo $BODY['header']; ?>
		
		<?php if(!empty($BODY['body'])) echo $BODY['body']; ?>

		<?php if(!empty($BODY['sidebar'])) echo $BODY['sidebar']; ?>
		
    </body>
</html>
