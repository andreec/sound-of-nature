<div class="footer">
	<div class="wrapper">
		 <div class="left">
		 	<a class="twit" target="_blank" href="http://twitter.com/OriginSourceID">Follow Us</a>
			<span>|</span>
			<a class="web" target="_blank" href="http://www.originalsource.co.id">Visit Our Website</a>
		 </div>
		 <div class="right">
		 	<a class="js-show-syarat">Syarat &amp; Ketentuan</a>
			<span>|</span>
			<a class="js-show-privacy">Kebijakan Privasi</a>
			<span>|</span>
			Copyright &copy; 2014. Original Source
		 </div>
	</div>
</div>
