<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" xmlns:fb="http://ogp.me/ns/fb#"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<base href="<?php echo $this->baseHref(); ?>">

 	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Sound of Nature</title>
	
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	
	<meta name="viewport" id="view1" content="width=640,initial-scale=0.5">
	
	<link rel="shortcut icon" href="assets/img/OSlogo.png" type="image/png">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">	
	<link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css" />
	
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-16370553-3', 'soundofnature.originalsource.co.id');
		ga('send', 'pageview');
	</script>
</head>
<body>
	<!-- Facebook JS API -->
	<div id="fb-root"></div>
	<script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '<?php echo $this->config("fb.appid"); ?>',
                xfbml      : true,
                version    : 'v2.2'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    
	<script>
	/*
	    window.fbAsyncInit = function () {
			FB.init({
				appId: '<?php echo $this->config('fb.appid'); ?>', // App ID
			});
			FB.Canvas.setAutoGrow(); //Resizes the iframe to fit content
		};
		
		// Load the SDK Asynchronously
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=<?php echo $this->config('fb.appid'); ?>";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	*/
		function login() {
		    FB.login(function(response) {
                if (response.authResponse) {
                    // var access_token = FB.getAuthResponse()['accessToken'];

                    FB.api('/me', function(response) {
						 $.ajax({
	                        url     : 'api/user/connect',
	                        data    : {
                        		'name' : response.name,
                        		'email': response.email,
                        		'fb_id': response.id
                    		},
	                        type    : 'post',
	                        success : function(result) {
	                        	console.log(result);
                        		xion.RouterManager.reroute(result.data.url);
                        		location.reload();	                           
	                        },
	                        error   : function(result) {
	                            //console.log(result.error.message);
	                        }
	                    });                    	
					});
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, {scope: 'email'});
		}
	</script>

	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
			 chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
	
	<div role="main" id="<?php echo (isset($body_id) || !empty($body_id)) ? $body_id : ''; ?>" class="content">
		
		<?php if(!empty($BODY['header'])) echo $BODY['header']; ?>
		<div class="transparentBG layout">
			<!-- <img class="note" src="assets/img/note-preloader.png"> -->
			<div class="preloader-content"><img src="assets/img/preloader.gif"></div>
			<div class="random-text">.....</div>
		</div>
		<div class="privacy">
			<div class="wrapper-privacy">
				<h1>Kebijakan Privasi</h1>
				<div class="privacy-wrapper">
					<div class="privacy-content">
						<p>Kami telah mengumpulkan, menggunakan, dan mengolah data pribadi dan informasi lainnya tentang penggunaan situs kami sesuai dengan Kebijakan Privasi kami. Dengan menggunakan situs kami, pengguna berarti setuju terhadap pengumpulan, penggunaan, dan pengolahan data tersebut.</p>
						<h5>Informasi Tentang Pengguna</h5>
						<p>Ketika pengguna mengunjungi halaman tertentu dari situs kami, kami akan meminta informasi tentang pribadi pengguna. Kami akan membutuhkan nama dan alamat e-mail anda juga alamat pos serta nomor telepon. Kami tidak akan mengumpulkan informasi tentang pengguna tanpa persetujuannya. Pada saat kami meminta informasi tentang diri pengguna sehubungan dengan promosi tertentu (misalnya, jika ada penawaran untuk memenangkan hadiah atau sampel produk gratis), jika pengguna tidak memberikan informasi tentang diri pengguna, pengguna mungkin tidak dapat berpartisipasi dalam promosi tersebut.</p>
						<h5>Penggunaan Website dan Hak Kekayaan Intelektual Kami</h5>
						<p>Kami telah membuat situs kami tersedia untuk diakses dan digunakan untuk kepentingan pribadi Anda. Kami dapat mengubah, menarik , atau menolak akses ke situs kami setiap saat. Situs kami dan semua materi yang terkandung di dalamnya dilindungi oleh hak kekayaan intelektual, termasuk hak cipta, untuk kami miliki dan gunakan sesuai dengan lisensi kami. Materi meliputi, tetapi tidak terbatas pada, desain, tata letak, gambaran, penampilan, grafis, dan dokumen di situs kami, serta konten lainnya seperti artikel dan teks lainnya. Merek dagang, logo, nama produk, dan nama merek yang digunakan di situs kami ("merek dagang kami") dimiliki oleh  PZ Cussons International Limited. Merek dagang kami tidak boleh digunakan pada atau berhubungan dengan produk atau layanan pihak ketiga tanpa persetujuan tertulis sebelumnya dari kami, dan tidak boleh digunakan pula dengan cara apapun yang membingungkan pelanggan atau mengindikasikan penghinaan. Anda tidak boleh menyalin, mendistribusikan, menerbitkan ulang, atau mengambil materi yang tersedia di situs kami untuk orang lain tanpa persetujuan kami secara tertulis. Anda tidak boleh men-download, menggunakan atau memanfaatkan seluruh atau sebagian dari situs kami beserta isinya untuk tujuan komersial atau menghasilkan uang tanpa izin tertulis sebelumnya dari kami. Anda tidak boleh mengubah seluruh atau sebagian dari situs kami beserta isinya atau mengklaim seluruh atau sebagian dari situs kami beserta isinya sebagai hasil pekerjaan Anda sendiri.</p>
						<p>Situs Hak Cipta &copy; 2014  PT PZ Cussons Indonesia, Hak Cipta Terjamin. Situs kami termasuk, tetapi tidak terbatas pada, desain, grafik, teks dan konten dinamis, dilindungi sesuai dengan Hak Cipta, Desain dan Paten Act 1988. Dilarang menggandakan sebagian atau keseluruhan situs tanpa izin tertulis dari  PT PZ Cussons Indonesia.</p>
					</div>
				</div>
				<div class="close js-close-privacy">TUTUP</div>
			</div>
		</div>

		<div class="syarat">
			<div class="wrapper-syarat">
				<h1>Syarat &amp; Ketentuan</h1>
				<div class="syarat-wrapper">
					<div class="syarat-content">
						<p>Pihak Penyelenggara atas dasar kebijakannya sendiri berhak untuk melakukan perubahan sewajarnya terhadap Syarat dan Ketentuan ini kapan pun, untuk melakukan kualifikasi dan verifikasi terhadap semua pengajuan keikutsertaan, dan untuk menolak setiap pengajuan keikutsertaan yang tidak memenuhi persyaratan sebagaimana yang ditetapkan oleh Pihak Penyelenggara. Dengan berpartisipasi, semua peserta dianggap telah menerima dan terikat oleh aplikasi dengan Syarat dan Ketentuan yang berlaku.Promosi ini tidak dipungut biaya apa pun. Hati-hati penipuan.</p>
						<hr>
						<h5>Syarat &amp; Ketentuan</h5>
						<h6>Permainan</h6>
						<p><span class="font-italic">Sound of Nature</span> adalah aplikasi permainan pada microsite terkoneksi dengan akun facebook peserta untuk mengajak peserta berkreasi dalam aransemen musik. Dalam periode 5 (lima) minggu pertama, yaitu periode 3 Desember 2014 -  9 Januari 2015, akan dipilih 1 pemenang, sesuai dengan ketentuan Pihak Penyelenggara. Periode 5 (lima) minggu selanjutnya, yaitu periode 2 Februari 2015 – 9 Maret 2015, akan dipilih 1 (satu) pemenang kembali, sesuai dengan ketentuan Pihak Penyelenggara. Peserta yang aransemen musiknya paling banyak didengar akan mendapatkan hadiah berupa 1 (satu) buah headphone Beats by Dre dan paket produk Original Source.</p>
						<h6>Peserta</h6>
						<p>
							Aplikasi ini terbuka bagi setiap orang yang memenuhi persyaratan berikut:
							<ol class="dec">
							  	<li>Warga Negara Indonesia (WNI)</li>
							  	<li>Bertempat tinggal di Republik Indonesia</li>
							  	<li>Wanita ataupun Pria berusia 17 tahun ke atas.</li>
							  	<li>Memiliki kartu Identitas diri yang masih berlaku (KTP atau Paspor atau SIM) sebagai verifikasi jika menjadi pemenang.</li>
							  	<li>Hadiah Permainan tidak berlaku bagi karyawan PT PZ Cussons Indonesia, agensi penyelenggara, agensi periklanan, serta siapa pun yang terkait secara profesional dengan permainan ini dan keluarga dekat mereka tidak diizinkan  untuk berpatisipasi.</li>
							  	<li>Untuk berpartisipasi, Peserta  harus memiliki akun Facebook pribadi yang sah untuk mengajukan aplikasi.</li>
							  	<li>Untuk berpartisipasi, Peserta harus merupakan pengguna Facebook dan masuk ke aplikasi <span class="font-italic">“Sound of Nature”</span>. Untuk membuat akun Facebook, silakan mendaftar di <a target="_blank" href="http://www.facebook.com">www.facebook.com</a></li>
							</ol>
						</p>
						<h6>Mekanisme Permainan</h6>
						<p>							
							<ol class="dec">
							  	<li>Peserta bergabung dalam aplikasi <span class="font-italic">Sound of Nature</span> dengan menggunakan akun Facebook peserta.</li>
							  	<li>Peserta dapat mengkreasikan aransemen musik sesuai dengan keinginan peserta.</li>
							  	<li>Peserta dapat membuat aransemen musik lebih dari 1 (satu) aransemen.</li>
							  	<li>Peserta dapat membagikan aransemen musik kreasi Peserta melalui akun media sosial Peserta seperti Facebook dan Twitter, untuk mengajak teman-teman Peserta mendengarkan hasil aransemen musik dari Peserta.</li>
							</ol>
						</p>
						<h6>Pemenang</h6>
						<p>							
							<ol class="dec">
							  	<li>Jumlah Pemenang adalah 2 (dua) Pemenang, dipilih berdasarkan aransemen musik yang paling banyak didengar dalam periode yang sudah ditentukan.</li>
							  	<li>Pemenang akan dipilih dalam 2 periode yang berbeda. Dalam periode 5 (lima) minggu pertama akan dipilih 1 (satu) Pemenang. Periode 5 (lima) minggu berikutnya akan dipilih kembali 1 (satu) Pemenang.</li>
							  	<li>Pengumuman Pemenang dilakukan maksimal 1 (satu) bulan setelah periode ditutup.</li>
							  	<li>Pemenang harus merupakan fans dari akun Facebook Original Source Indonesia dan atau merupakan followers dari akun Twitter Original Source Indonesia, yaitu <a href="https://twitter.com/OriginSourceID" target="_blank">@OriginSourceID</a></li>
							  	<li>
							  		Pemenang wajib mengirimkan data diri berupa:
							  		<ul class="disc">
										<li>Nama Lengkap</li>
										<li>Alamat pengiriman hadiah</li>
										<li>Nomor yang dapat dihubungi</li>
										<li>Hasil pindai indentitas diri, bisa berupa KTP, SIM atau pun Paspor.</li>
										<li>Bukti bahwa Pemenang adalah fans dan atau followers akun media sosial Original Source Indonesia</li>
									</ul>
							  	</li>
							  	<li>Batas waktu verifikasi data pribadi Pemenang adalah 1 (satu) minggu sejak pengumuman di wall fanpage Original Source Indonesia. Jika Pemenang tidak melakukan verifikasi data pribadi dalam kurun waktu tersebut, pihak Penyelenggara berhak mendiskualifikasi Pemenang dan menggantikannya dengan Peserta lain yang berhak menjadi Pemenang.</li>
							</ol>
						</p>
						<h6>Seleksi dan Penjurian</h6>
						<p>							
							<ol class="dec">
							  	<li>Tidak ada satu Peserta pun yang boleh menang lebih dari satu kali.</li>
							  	<li>Keputusan juri adalah final dan tidak dapat diganggu gugat.</li>
							</ol>
						</p>
						<h6>Hadiah</h6>
						<p>							
							<ol class="dec">
							  	<li>Total hadiah berupa 2 (dua) buah Headphone Beats by Dre, warna tergantung ketersediaan produk, dan paket produk Original Source untuk total 2 (dua) Pemenang yang terbagi dalam 2 (dua) tahap periode.</li>
							  	<li>Hadiah tidak dapat dialihkan, ditukar barang lain, atau diganti dengan uang.</li>
							  	<li>Hadiah akan dikirimkan ke alamat yang telah dikonfirmasi oleh Pemenang saat verifikasi data.</li>
								<li>Pengiriman hadiah akan dilakukan dalam kurun waktu maksimal 1 (satu) bulan setelah Pemenang verifikasi data.</li>
								<li>Jika karena alasan apapun pemberian hadiah tidak dapat berjalan sesuai rencana, Pihak Penyelenggara memiliki hak untuk memodifikasi ketentuan pemberian hadiah yang senilai dengan hadiah yang telah dijanjikan.</li>
							</ol>
						</p>
						<h6>Perhatian/Catatan</h6>
						<p>							
							<ol class="dec">
							  	<li>Memberikan informasi palsu dan/atau membuat akun Twitter lain untuk memperbesar kemungkinan menang dan/atau melakukan tindakan-tindakan yang dianggap sebagai kecurangan terhadap mekanisme permainan ini merupakan tindakan yang dianggap pelanggaran terhadap permainan ini dan akan didiskualifikasi.</li>
							  	<li>Siapa pun yang diketahui mengganggu proses atau bertindak dengan segala sikap yang oleh Pihak Penyelenggara dianggap melanggar Syarat dan Ketentuan berlaku akan didiskualifikasi.</li>
							</ol>
						</p>
						<h6>Lain-lain</h6>
						<p>							
							<ol class="dec">
							  	<li>Komentar, pertanyaan, ataupun tanggapan lebih lanjut mengenai aplikasi <span class="font-italic">Sound of Nature</span> ini dapat disampaikan melalui Facebook Original Source (Original Source Indonesia) atau melalui Contact Us di Website Original Source <a target="_blank" href="http://www.originalsource.co.id">(www.originalsource.co.id)</a>. Pengguna harus memberikan keterangan sebagai berikut untuk dapat ditanggapi: Nama, alamat email, nomor telepon / handphone yang dapat dihubungi dan alamat tempat tinggal saat ini.</li>
							  	<li>Pihak penyelenggara berhak meliput, mendokumentasikan (termasuk namun tidak terbatas mengambil foto dan/atau video) acara pemberian hadiah, untuk digunakan bagi segala keperluan Original Source tanpa biaya tambahan apapun.</li>
							  	<li>Dengan mengikuti kegiatan ini, Peserta memberikan hak kepada pihak Original Source untuk mengedit, menggandakan, dan menyebarluaskan hasil karya yang Peserta melalui social media atau website pihak Original Source.</li>
							</ol>
						</p>
					</div>
				</div>
				<div class="close js-close-syarat">TUTUP</div>
			</div>
		</div>

		<div class="reloadnotif">
			<div class="wrapper-reloadnotif">
				<div class="reloadnotif-wrapper">Sorry, kamu harus mengulang karyamu karena kamu keluar dari halaman ini. Get ready to make another intense track!</div>
				<div class="close js-close-reload">OK</div>
			</div>
		</div>

		<div id="bodyselector" class="body<?php echo (isset($class)) ? ' '.$class : ''; ?><?php echo (isset($backgroundtype)) ? ' '.$backgroundtype : ''; ?>"> <!-- class="no-login" -->
			<div class="mainbg">
				<img class="bg bg-park" src="assets/img/bg/main.jpg" alt="" />
				<img class="bg bg-waterfall" src="assets/img/bg/waterfall.jpg" alt="" />
				<div class="ornament">
					<img class="orn left" src="assets/img/bg/forest-ornament-left.png"/>
					<img class="orn right" src="assets/img/bg/forest-ornament-right.png"/>
					<img class="orn mid" src="assets/img/bg/forest-ornament-mid.png"/>
				</div>
			</div>

			<div id="maincontent">
				
			</div>

		</div>
		
		<?php if(!empty($BODY['footer'])) echo $BODY['footer']; ?>
		
	</div>
	
	
	<!-- custom alert template -->
	<div class="err-alert">
		<div class="inner"></div>
		<div class="dismiss">(click to dismiss)</div>
	</div>
	
	
	<!-- popup template -->
	<div class="overlay"></div>
	<div class="popup" id="popup-fbconnect">
		<div class="inner">
			Popup Test
		</div>
		<p class="popup-close">X</p>
	</div>
	
	
	<!-- loading overlay template -->
	<div class="nowloading"></div>
	<div class="nowloading-icon"></div>

    <!-- JavaScript at the bottom for fast page loading -->
	<script src="assets/ori/js/libs/jquery.1.10.2.min.js"></script>	
	<!-- custom scrollbar plugin -->
	<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="assets/ori/js/libs/createjs/utils/Proxy.js"></script>
	<script src="assets/ori/js/libs/createjs/utils/IndexOf.js"></script>
	<script src="assets/ori/js/libs/createjs/events/Event.js"></script>
	<script src="assets/ori/js/libs/createjs/events/EventDispatcher.js"></script>
    <script src="assets/ori/js/libs/soundjs/Sound.js"></script>
    <script src="assets/ori/js/libs/soundjs/WebAudioPlugin.js"></script>
    <script src="assets/ori/js/libs/soundjs/HTMLAudioPlugin.js"></script>
    <script src="assets/ori/js/libs/soundjs/swfobject.js"></script>
    <script src="assets/ori/js/libs/soundjs/FlashPlugin.js"></script>
	<script src="assets/ori/js/libs/underscore.min.js"></script>
	<script src="assets/ori/js/libs/interact.min.js"></script>	
	<script src="assets/js/script.min.js"></script>
	<!-- end scripts -->
</body>
</html>
