<?php

class User {

    private $orm;
    
    public function __construct() {
        $db = new Database(include PATH_APP.'/config.inc.php');
        
        $this->orm = $db->orm();
    }

    public function retrieve($id = null) {
        if (!isset($id)) 
            return $this->orm->user();
        
        return $this->orm->user[$id];
    }
    
    public function register($data) {
        $user = $this->orm->user()->where('fb_id', $data['fb_id'])->fetch();
        if (!$user) {
            $insertUser = $this->orm->user()->insert(array(
                'name' => $data['name'],
                'email' => $data['email'],
                'fb_id' => $data['fb_id'],
                'registered' => date('YmdHis'),
                'is_active' => true,
            ));
            return array(
                'status' => 'register',
                'data' => array(
                    'id' => $insertUser['id']
                ),
            );
        } else {
            return array(
                'status' => 'login',
                'data' => array(
                    'id' => $user['id'],
                    'name' => $user['name'],
                ),
            );
        }
    }

    public function updateUserdata($userID,$data) {
        $user = $this->orm->user()->where('id',$userID)->fetch();
        if ($user) {
            $updateUser = $this->orm->user()->where('id',$userID)->update($data);
            if($updateUser) {
                return true;
            } else {
                return false;
            }
        }
    }
}

?>
