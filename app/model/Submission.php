<?php

class Submission {

    private $orm;
    
    public function __construct() {
        $db = new Database(include PATH_APP.'/config.inc.php');
        $this->orm = $db->orm();
    }

    public function getDetail($submissionID) {
        $submission = $this->orm->user_submission()
            ->where('id',$submissionID)
            ->fetch();

        $user = $this->orm->user()->where('id',$submission['user_id'])->fetch();
        $submission_data = array(
            'title' => ctype_space($submission['title']) || empty($submission['title']) ? 'Untitled' : $submission['title'],
            'total_listener' => $submission['total_listener'],
            'created' => $submission['created'],
            'record_data' => $submission['record_data'],
            'userdata' => array(
                'name' => $user['name'],
                'email' => $user['email'],
                'fb_id' => $user['fb_id'],
            )
        );

        return $submission_data;
    }

    public function getTopSong() {
        $submission_data = array();
        $submission = $this->orm->user_submission()
        	->order('total_listener DESC')
        	->limit(3);
        foreach ($submission as $row) {
        	$user = $this->orm->user()->where('id', $row['user_id'])->fetch();
        	$submission_data[] = array(
                'id' => $row['id'],
        		'title' => ctype_space($row['title']) || empty($row['title']) ? 'Untitled' : $row['title'],
        		'total_listener' => $row['total_listener'],
        		'created' => $row['created'],
        		'record_data' => $row['record_data'],
        		'userdata' => array(
        			'name' => $user['name'],
            		'email' => $user['email'],
            		'fb_id' => $user['fb_id'],
        		)
        	);
        }
        return $submission_data;
    }

    public function getAll($limit=null,$start=null, $title=null, $listened=null, $composer=null, $search=null){
        if (empty($limit) and empty($start)) {
            $submission = $this->orm->user_submission();
            $total = count($submission);
        } else {
            if ($title!=null) {
                $submission = $this->orm->user_submission()
                    ->limit($limit,$start)
                    ->order("title $title");
                    
                $total = $this->orm->user_submission()->count();
            } else if ($listened!=null) {
                $submission = $this->orm->user_submission()
                    ->limit($limit,$start)
                    ->order("total_listener $listened");
                    
                $total = $this->orm->user_submission()->count();
            } else if ($composer!=null) {
                $submission = $this->orm->user_submission("user.id",$this->orm->user())
                    ->limit($limit,$start)
                    ->order("name $composer");
                    
                $total = $this->orm->user_submission("user.id",$this->orm->user())->count();
            } else if ($search!=null) {
                $submission = $this->orm->user_submission()
                    ->where('title LIKE ?','%'.$search.'%')
                    ->or('user_id', $this->orm
                        ->user()
                        ->select('id')
                        ->where('name LIKE ?', '%'.$search.'%'))
                    ->or('total_listener LIKE ?', '%'.$search.'%')
                    ->limit($limit,$start);
                    
                $total = $this->orm->user_submission()
                    ->where('title LIKE ?','%'.$search.'%')
                    ->or('user_id', $this->orm
                        ->user()
                        ->select('id')
                        ->where('name LIKE ?', '%'.$search.'%'))
                    ->or('total_listener LIKE ?', '%'.$search.'%')
                    ->count();
            } else {
                $submission = $this->orm->user_submission()
                    ->limit($limit,$start)
                    ->order("title $title");
                    
                $total = $this->orm->user_submission()->count();
            }
        }
    	
        foreach ($submission as $row) {
            $user = $this->orm->user()->where('id',$row['user_id'])->fetch();
            $submission_data[] = array(
                'id' => $row['id'],
                'title' => ctype_space($row['title']) || empty($row['title']) ? 'Untitled' : $row['title'],
                'total_listener' => $row['total_listener'],
                'created' => $row['created'],
                'record_data' => $row['record_data'],
                'userdata' => array(
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'fb_id' => $user['fb_id'],
                )
            );
        } 
        
        $submission_data['total'] = $total;

        return $submission_data;
    }

    public function insert($data) {
        $insertSubmission = $this->orm->user_submission()->insert(array(
            'user_id' => $data['user_id'],
            'title' => ctype_space($data['title']) || empty($data['title']) ? 'Untitled' : $data['title'],
            'total_listener' => 0,
            'created' => date('YmdHis'),
            'is_active' => true,
            'record_data' => $data['record_data'],
        ));
        
        return $insertSubmission;
    }

    public function counter($data) {
        $listener = new Listener();
        
        if (!empty($data['user_id']) && !$listener->isExists($data['user_id'], $data['id'])) {
        
            $listener->insert($data);
            
            $submission = $this->orm->user_submission()
                ->where('id',$data['id'])
                ->fetch();
            
            if ($submission) {
                $submission['total_listener'] += 1;
                $submission->update();
                
                return $submission['total_listener'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
?>
