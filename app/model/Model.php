<?php

abstract class Model {
    
    public function retrieve() {
        
    }
    
    abstract public function insert();
    abstract public function update();
    abstract public function delete();
}

?>
