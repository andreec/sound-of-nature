<?php

class Listener {

    private $orm;
    
    public function __construct() {
        $db = new Database(include PATH_APP.'/config.inc.php');
        $this->orm = $db->orm();
    }

    public function insert($data) {
        if (!$this->isExists($data['user_id'], $data['user_submission_id'])) {
            $row = $this->orm->user_listener()->insert(array(
                'user_id' => $data['user_id'],
                'user_submission_id' => $data['user_submission_id'],
                'created' => date('YmdHis')
            ));
            
            if ($row)
                return true;
                
            return false;
        }
        
        return false;
    }
    
    public function isExists($user_id = null, $user_submission_id = null) {
        if (empty($user_id) || empty($user_submission_id)) return false;
        
        $listener = $this->orm->user_listener()
            ->where('user_id', $user_id)
            ->where('user_submission_id', $user_submission_id)
            ->fetch();
        
        if (!$listener) return false;
        
        return true;
    }
}
?>
