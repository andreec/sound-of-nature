$(function() {

    defaultContent = '\
        <div class="btn-group btn-group-xs file_actions pull-center">\
	        <a class="btn btn-default edit-btn member" title="Edit">\
		        <i class="fa fa-pencil"></i>\
	        </a>\
	        <a class="btn btn-default delete-btn member" title="Remove">\
			    <i class="fa fa-trash-o"></i>\
		    </a>\
        </div>\
    ';
    
    defaultDelete = '\
        <div class="btn-group btn-group-xs file_actions pull-center">\
	        <a class="btn btn-default delete-btn submission" title="Remove">\
			    <i class="fa fa-trash-o"></i>\
		    </a>\
        </div>\
    ';
    
	$('#dt-user-member').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "cms/api/user/pagination",
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "fb_id" },
            { "data": "registered" },
            { "data": "is_active" },
            { "data": null},
            { "data": "id" }
        ],
        "columnDefs": [
            {
                "targets": -2,
                "data": null,
                "defaultContent": defaultContent
            },
            {
                "targets": -1,
                "visible": false,
                "searchable": false
            }
        ]
    });
    
    $('#dt-user-submission').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "cms/api/submission/pagination",
        "columns": [
            { "data": "name" },
            { "data": "title" },
            { "data": "total_listener" },
            { "data": "created" },
            { "data": "is_active" },
            { "data": null },
            { "data": "id" }
        ],
        "columnDefs": [
            {
                "targets": -2,
                "data": null,
                "defaultContent": defaultDelete
            },
            {
                "targets": -1,
                "visible": false,
                "searchable": false
            }
        ]
    });
    
    $('#dt-user-listener').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "cms/api/listener/pagination",
        "columns": [
            { "data": "title" },
            { "data": "listener" },
            { "data": "created" },
            { "data": "id" }
        ],
        "columnDefs": [
            {
                "targets": -1,
                "visible": false,
                "searchable": false
            }
        ]
    });
});
