$(function() {
    tableMember = $('#dt-user-member').dataTable();
    
	$('body').on('click', '.edit-btn', function() {
	
	    var user = tableMember.fnGetData($(this).parents('tr'));
        
		$.ajax({
            type: 'get',
            url: 'cms/api/file/user-member',
            data: {'id': user.id},
            dataType: 'json',
            success: function(data) {
                bootbox.dialog({
                    message: data.content,
                    title: "",
                    buttons: {
                        success: {
                            label: "Save",
                            className: "btn-success",
                            callback: function() {
                                $("#form-member").submit(function() {
                                    $.ajax({
                                        type: "POST",
                                        url: "cms/api/user/update",
                                        data: $("#form-member").serialize(), // serializes the form's elements.
                                        success: function(data) {
                                            if (data.status == 'succeed')
                                                location.reload();
                                        }
                                    });

                                    return false; // avoid to execute the actual submit of the form.
                                });
                                
                                $("#form-member").submit();
                                
                            }
                        },
                        main: {
                            label: "Cancel",
                            className: "btn-primary"
                        }
                    }
                });
            }
        });
	});
	
	tableSubmission = $('#dt-user-submission').dataTable();
	
	$('body').on('click', '.delete-btn.submission', function() {
	    var submission = tableSubmission.fnGetData($(this).parents('tr'));
	    
	    bootbox.confirm('Do you want to delete submission with title <strong>' + submission.title + '</strong>, from author <strong>' + submission.name + '</strong>?', function(result) {
            if (result) {
                $.ajax({
	                type: 'post',
	                url: 'cms/api/submission/delete',
	                data: {'id': submission.id},
	                dataType: 'json',
	                success: function(data) {
	                    if (data.status == 'succeed') {
	                        location.reload();
	                    }
	                }
	            });
            }
        }); 
	});
	
});
