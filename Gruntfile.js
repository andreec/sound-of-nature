    /**
     * Gruntfile.js
     */
    module.exports = function(grunt) {
      grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        phpunit: {
            unit: {
                dir: 'app/test'
            },
            options: {
                bin: 'phpunit',
                bootstrap: 'bootstrap/start.php',
                colors: true,
                testdox: true
            }
        },
        watch: {
          configFiles: {
            files: [ 
                'Gruntfile.js', 
                'app/model/*',
                'app/lib/*',
                'app/test/*'
            ],
            tasks: [ 'default' ],
            options: {
                livereload: true,
                nospawn: true
            },
            css: {
                files: [ "work/<%= pkg.dir %>/assets/css/**/*" ],
                // files: [ "work/<%= pkg.dir %>/assets/ori/sass/**/*" ],
                // tasks: [ "compass" ]
                // tasks: [ "newer:compass" ]
            },
            js: {
                files: [ "work/<%= pkg.dir %>/assets/ori/js/*" ],
                tasks: [ "uglify" ]
            },
            img: {
                files: [ "work/<%= pkg.dir %>/assets/img/**/*" ]
            },
            file: {
                files: [ "work/<%= pkg.dir %>/app/view/**/*" ]
            },
          }
        }
      });

      grunt.loadNpmTasks('grunt-phpunit');
      grunt.loadNpmTasks('grunt-contrib-watch');
      grunt.registerTask('default', ['phpunit:unit']);
      grunt.registerTask("startme", [ "newer:compass", "uglify", "cssmin", "connect:server", "prettify", "watch" ]), 
      grunt.registerTask("startmewocompass", [ "uglify", "cssmin", "connect:server", "watch" ]), 
      grunt.registerTask("indam", [ "compass", "uglify", "cssmin", "connect:server", "watch" ]), 
      grunt.registerTask("jalan", [ "compass", "jade", "watch" ]),
      grunt.registerTask("misal", [ "uglify", "watch" ]), 
      grunt.registerTask("css", [ "compass", "watch" ]),
      grunt.registerTask("ab", [ "concat", "jshint", "watch" ]), 
      grunt.registerTask("mini", [ "cssmin", "watch" ]),
      grunt.registerTask("cn", [ "connect:server", "watch" ]), 
      grunt.registerTask("cp", [ "copy" ]);
      grunt.registerTask("abc", [ "prettify" ]);
    };
