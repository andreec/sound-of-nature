/*
Developer: Xion 
Name: OS
Version: 0.1.1
Last Compile: 2014-10-16 */

var xion = function() {
    function validateContactForm() {
        if ("" == $("#user-data #name").val()) {
            $("#user-data #name").addClass("error").next(".error-msg").html("Anda belum mengisi nama").show();
        } else {
            $("#user-data #name").removeClass("error").next(".error-msg").html("").hide();
        }
        if ("" == $("#user-data #telp").val()) {
            $("#user-data #telp").addClass("error").next(".error-msg").html("Anda belum mengisi nomor telepon").show();
        } else {
            if (!numReg.test($("#user-data #telp").val())) {
                $("#user-data #telp").addClass("error").next(".error-msg").html("Nomor telepon harus angka").show();
            } else {
                if ($("#user-data #telp").val().length < 7) {
                    $("#user-data #telp").addClass("error").next(".error-msg").html("Nomor telepon minimal 7 digit").show();
                } else {
                    $("#user-data #telp").removeClass("error").next(".error-msg").html("").hide();
                }
            }
        }
        if ("" == $("#user-data #email").val()) {
            $("#user-data #email").addClass("error").next(".error-msg").html("Anda belum mengisi alamat email").show();
        } else {
            if (!emailReg.test($("#user-data #email").val())) {
                $("#user-data #email").addClass("error").next(".error-msg").html("Alamat email tidak valid").show();
            } else {
                $("#user-data #email").removeClass("error").next(".error-msg").html("").hide();
            }
        }
    }
    function validateContact() {
        var validated = 0;
        $('.contact form input[type="submit"]').on("click", function() {
            validated = 1;
            validateContactForm();
            if ($(".contact form .error").length > 0) {
                return false;
            }
        });
        $(".contact form input").blur(function() {
            if (1 == validated) {
                validateContactForm();
            }
        });
    }
    function customInputFile() {
        $("input[type=file]").val("");
        $("nput[type=file]").on("change", function() {
            var fileName = $(this).val();
            sizeupload = this.files[0].size;
            var splitName = fileName.split(":\\fakepath\\");
            if (splitName.length > 1) {
                $(this).prev(".filenameTxt").html(splitName[1]);
            } else {
                $(".filenameTxt").html(splitName[0]);
            }
        });
    }
    function socialShares() {
        var title;
        var shareURL;
        var imgURL;
        var caption;
        var description;
        $(".btnbelow .shareleft").on("click", function() {
            title = $(".jdlrecipe .recipe-name").html();
            shareURL = "http://stag.xituz.com/quaker/inspirasi-sahur-sehat/";
            imgURL = "http://stag.xituz.com/quaker/inspirasi-sahur-sehat/" + $("#header > img").attr("src");
            caption = "Sahur Inspiration";
            description = "Temukan inspirasi sahur sehat lezat, dengan bahan-bahan yang pasti sehat untuk sekeluarga.";
            FB.ui({
                method: "feed",
                name: title,
                link: shareURL,
                picture: imgURL,
                caption: caption,
                description: description
            }, function(response) {
                if (response) {
                    console.log("Share success!");
                }
            });
        });
        var sendURL;
        $(".share-pesan .share.sendit").on("click", function() {
            sendURL = "http://bit.ly/101Ekspresi";
            FB.ui({
                method: "send",
                link: sendURL
            });
        });
        var twitText;
        var twitURL;
        var completeTwitURL;
        $(".share .twit").on("click", function() {
            twitText = "Serunya ikutan Ekspresi 101 Rasa Ramadan ABC Special Grade, dan jangan lupa kirim pesan semangat Ramadan-nya! Join yuk!";
            twitURL = "https://stag.xituz.com/heinz/cocopandan/";
            completeTwitURL = "https://twitter.com/intent/tweet?text=" + twitText + "&url=" + twitURL;
            $(this).attr({
                href: completeTwitURL,
                target: "_blank"
            });
        });
        FB.getLoginStatus(function(response) {
            console.log(response.status);
            if ("connected" === response.status) {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;
                console.log(uid);
                console.log(accessToken);
            } else {
                if ("not_authorized" === response.status) {
                    login();
                } else {
                    login();
                }
            }
        });
    }
    function showPopup(popupID, cscroll) {
        $(".overlay").fadeIn(300);
        $(".popup#" + popupID).fadeIn(600);
        if (cscroll || true == cscroll) {
            $(".popup#" + popupID + " .cscroll").jScrollPane();
            if (cscroll || true == cscroll) {
                $(".cscroll").jScrollPane({
                    autoReinitialise: true,
                    verticalDragMinHeight: 54,
                    verticalDragMaxHeight: 54,
                    mouseWheelSpeed: 50,
                    animateScroll: true
                });
            }
        }
    }
    function popups() {
        $(".show-popup").on("click", function() {
            var id = $(this).attr("data-popup");
            showPopup(id, true);
            return false;
        });
        $(".popup-close,.overlay").on("click", function() {
            $(".popup").fadeOut(300);
            $(".overlay").fadeOut(600);
        });
        if ($(".popup.auto").length > 0) {
            var id = $(".popup.auto").attr("id");
            showPopup(id);
        }
    }
    function _checkCheckbox() {
        $(".checkbox, .radio").each(function() {
            if (true == $(this).find("input").prop("checked")) {
                $(this).addClass("checked");
            } else {
                $(this).removeClass("checked");
            }
        });
    }
    function customCheckbox() {
        $('input[type="checkbox"]').each(function() {
            if (true == $(this).parent().hasClass("custom")) {
                $(this).parent().addClass("checkbox");
            }
        });
        $('input[type="radio"]').each(function() {
            if (true == $(this).parent().hasClass("custom")) {
                $(this).parent().addClass("radio");
            }
        });
        _checkCheckbox();
        $(".checkbox, .radio").click(function() {
            _checkCheckbox();
        });
    }
    function showAlert(errmsg, auto, stayTime, fadeTime) {
        if ("true" == auto || void 0 === auto) {
            $(".err-alert").removeClass("dismissable");
            if (void 0 === stayTime) {
                stayTime = 1e3;
            }
            if (void 0 === fadeTime) {
                fadeTime = 3e3;
            }
        } else {
            if ("false" == auto) {
                $(".err-alert").addClass("dismissable");
            }
        }
        $(".err-alert").stop(true, true).fadeIn(200, function() {
            if ("true" == auto || void 0 === auto) {
                $(".err-alert").fadeTo(stayTime, 1, function() {
                    $(".err-alert").fadeOut(fadeTime, function() {
                        $(".err-alert .inner").html("");
                    });
                });
            }
        });
        $(".err-alert .inner").html(errmsg);
    }
    function notifAlert() {
        $(".show-alert").on("click", function() {
            var msg = $(this).attr("data-alert");
            var auto = $(this).attr("data-auto");
            var stayTime = 800;
            var fadeTime = 2500;
            showAlert(msg, auto, stayTime, fadeTime);
        });
        $(".err-alert").on("click", function() {
            if (true == $(this).hasClass("dismissable")) {
                $(".err-alert").fadeOut(300);
            }
        });
    }
    function centerImages(selector) {
        $(window).width();
        $(window).height();
        $(selector).each(function() {
            var img = $(this).find("img");
            var src = img.attr("src");
            img.attr("src", "");
            img.attr("src", src);
            img.load(function() {
                img.css({
                    width: "auto",
                    height: "auto"
                });
                var imgW = img.width();
                var imgH = img.height();
                var boxW = $(this).parent().width();
                var boxH = $(this).parent().height();
                if (imgW / imgH > boxW / boxH) {
                    img.css({
                        height: "100%",
                        width: "auto"
                    });
                    imgW = img.width();
                    img.css({
                        "margin-top": 0,
                        "margin-left": (boxW - imgW) / 2
                    });
                } else {
                    img.css({
                        width: "100%",
                        height: "auto"
                    });
                    imgH = img.height();
                    img.css({
                        "margin-left": 0,
                        "margin-top": (boxH - imgH) / 2
                    });
                }
            });
        });
    }
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var numReg = /^[0-9]+$/;
    var sizeupload;
    var selectedHowto = new Array();
    $('#existinggaruda input[name="howto[]"]:checked').each(function() {
        selectedHowto.push($(this).val());
    });
    var datasend = {
        email: $('#existinggaruda input[name="email"]').val(),
        password: $('#existinggaruda input[name="password"]').val(),
        howto: selectedHowto
    };
    $(".sample-loading-overlay").on("click", function() {
        $(".nowloading, .nowloading-icon").show();
        $.ajax({
            type: "POST",
            url: "login-garuda",
            data: datasend,
            dataType: "json",
            success: function(data) {
                setTimeout(function() {
                    $(".nowloading, .nowloading-icon").hide();
                    if ("succeed" == data.result) {
                        showAlert("yaayy success!!");
                    } else {
                        var errmsg = data.message;
                        showAlert(errmsg);
                    }
                }, 3e3);
            },
            error: function() {
                console.log("ok");
                setTimeout(function() {
                    $(".nowloading, .nowloading-icon").hide();
                    showAlert("AJAX request failed...");
                }, 3e3);
            }
        });
    });
    return {
        init: function() {
            customCheckbox();
            centerImages(".container .box");
            notifAlert();
            popups();
            socialShares();
            validateContact();
            customInputFile();
        }
    };
}();

$(document).ready(function() {
    xion.init();
});