jQuery.fn.forceNumeric = function () {
	return this.each(function () {
		$(this).keydown(function (e) {
			var key = e.which || e.keyCode;

			if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
			// numbers   
			key >= 48 && key <= 57 ||
			// Numeric keypad
			key >= 96 && key <= 105 ||
			// String 
				key >= 65 && key <= 90 ||   
			// Dash
				key == 189 ||   
			// Space
				key == 32 ||                                             
			// Backspace and Tab and Enter
			key == 8 || key == 9 || key == 13 ||
			// Home and End
			key == 35 || key == 36 ||
			// left and right arrows
			key == 37 || key == 39 ||
			// Del and Ins
			key == 46 || key == 45
			)
		    {
		    	return true;
		    }
		    else if (e.shiftKey &&
		    	// String
		     	key >= 65 && key <= 90 ) {
		    	return true;	
		    }                    
		    else {
		    	return false;	
		    }
		});
	});
}

var xion = (function(){

	var _allsoundloaded = false,
		_baseHref = $('base').attr('href'),
		RouterManager,
		SoundPlayer,
		SliderManager,
		ClickManager,
		Draggable;
		
	/* ============ centering image horizontally and vertically (background-size:cover equivalent) ============ */
	
	function _checkCheckbox(){		
		$('.checkbox, .radio').each(function(){
			if ($(this).find('input').prop('checked') == true){
				$(this).addClass('checked');
			}
			else {
				$(this).removeClass('checked');
			}
		});
	}
	function customCheckbox(){
		$('input[type="checkbox"]').each(function(){
			if ($(this).parent().hasClass('custom') == true){
				$(this).parent().addClass('checkbox');
			}
		});
		$('input[type="radio"]').each(function(){
			if ($(this).parent().hasClass('custom') == true){
				$(this).parent().addClass('radio');
			}
		});
		_checkCheckbox();
		$('body').on('click','.checkbox',function(){
		//$('.checkbox, .radio').click(function(){
			_checkCheckbox();
		});	
	}

	function centerImages(selector){
		var winW = $(window).width();
		var winH = $(window).height();
		var imgW;
		var imgH;
		var img;
		var boxW;
		var boxH;
		// based on parent container size
		$(selector).each(function(){
			var img = $(this).find('img');
			var src = img.attr('src');
			img.attr('src','');
			img.attr('src',src);
			img.load(function(){
				img.css({'width':'auto','height':'auto'});
				var imgW = img.width();
				var imgH = img.height();
				var boxW = $(this).parent().width();
				var boxH = $(this).parent().height();
				if (imgW/imgH > boxW/boxH){
					img.css({'height':'100%','width':'auto'});
					imgW = img.width();
					img.css({'margin-top':0,'margin-left':(boxW-imgW)/2});
				}
				else {
					img.css({'width':'100%','height':'auto'});
					imgH = img.height();
					img.css({'margin-left':0,'margin-top':(boxH-imgH)/2});
				}
			});
		});
	}

	//search	
	function galleryAjax(keyword,page,category,order){						
		console.log('test');
		$.ajax({
			url : _baseHref +'api/gallery',
			type : 'GET',
			dataType : 'json',
			data : {
				keyword : keyword,
				page : page,
				category : category,
				order : order
			},
			success : function(content){		
				console.log(content);						
				if (content == '' || content == null || !content){
					$('.gallery-list').html('<p class="not-found">Pencarian tidak ditemukan</p>');
					$('.gallery-bottom .nav').html('');
				}
				else {					
					$('.gallery-list').html('');
					content.gallery.forEach(function(item) {
						var li = '<li class="list hover">' +
							'<div class="gal-img">' +
								'<img src="http://graph.facebook.com/' + item.photo + '/picture?type=square" alt="' + item.name + '">' +
							'</div>' +
							'<div class="gal-title">' +
								item.title +
							'</div>' +
							'<div class="gal-composer">' +
								item.name +
							'</div>' +
							'<div class="gal-listen">' +
								item.listen +
							'</div>' +
							'<div class="gal-play"></div>' +
							'<a class="play" href="#track_' + item.submission_id + '"></a>' +
						'</li>'
						$('.gallery-list').append(li);
					});

					var activePage = content.pagination.active;

					$('.gallery-bottom .nav').html('');
					if(content.pagination.stop!=1) {
						if (activePage!= 1){
							$('.gallery-bottom .nav').append('<a class="prev"> &lt; </a>');
						}
						for (i=content.pagination.start ; i<=content.pagination.stop ; i++){
							var active = '';
							if (content.pagination.active == i){
								active = 'active'
							}
							var pagination = '<a class="' + active + '"> ' + i + ' </a>'
							$('.gallery-bottom .nav').append(pagination);
						}					
						if (activePage != content.pagination.stop){
							$('.gallery-bottom .nav').append('<a class="next"> &gt; </a>');
						}								
					}					
				}
			},
			error : function(){
				// window.location.reload();
			}
		});
		return false;
	}

	function gallerySearch(){
		var keyword = '';
		var page = 1;
		var category = '';
		var order = 'ascending';

		$('body').on('click','.new-title',function(){			
			category = $(this).attr('data-title');			
			if ($(this).attr('data-order') == 'ascending'){
				order = 'descending';
			}
			else {
				order = 'ascending';
			}
			galleryAjax(keyword,page,category,order);
			return false;			
		});


		$('body').on('click','.new-composer',function(){
			category = $(this).attr('data-title');
			if ($(this).attr('data-order') == 'ascending'){
				order = 'descending';
			}
			else {
				order = 'ascending';
			}
			galleryAjax(keyword,page,category,order);
			return false;			
		});

		$('body').on('click','.new-listen',function(){
			category = $(this).attr('data-title');
			if ($(this).attr('data-order') == 'ascending'){
				order = 'descending';
			}
			else {
				order = 'ascending';
			}
			galleryAjax(keyword,page,category,order);
			return false;
		});		
		
		$('body').on('submit','.gallery-form',function(){							
			keyword = $(this).find('input[name=search]').val();
			page = 1;			
			galleryAjax(keyword,page,category,order);

			return false;
		});	

		$('body').on('click', '.nav a', function(event) {			
			//console.log('"'+$(this).text()+'"');		
			page = $(this).text();	
			activePage = $('.nav').find('.active').text();
			if(page==" > ") {
				page = parseInt(activePage)+1;
			}
			else if(page==" < ") {
				page = parseInt(activePage)-1;
			}
			else {
				page = $(this).text();	
			}						

			galleryAjax(keyword,page,category,order);			
			return false;

		});		
	}

	RouterManager = (function(){

		var _currentRoute,
			_mainWrapper = $('#maincontent'),
			_isLoaded = false,
			_isLoading = false,
			_bodySelector = $('#bodyselector'),
			_maindiv = $('div[role=main]'),
			_contentconfig = '#contentdata',
			_config,
			_baseHref = $('base').attr('href'),
			_initcallback = {
				playground : function(){				
					Draggable.init();
					SliderManager.init();
					_checkFirstLogin();
					$("input").forceNumeric();
				},
			};

		function _checkRoute(initial) {

			_currentRoute = window.location.href.substring(window.location.href.lastIndexOf('/')+1) || window.location.hash || 'home';

			if(_currentRoute != '' && _currentRoute.indexOf('#') >= -1) {
				_currentRoute = _currentRoute.substring(_currentRoute.indexOf('#')+1);
			}

			switch(_currentRoute) {
				case 'connect' :
				case 'like' :
				case 'gallery' :
				case 'playground' : 														
				case 'register' :
				case 'home' :					
					_displayMainContent();
					_homeLoad();
					
				break;
				default :
					if(initial) {
						_currentRoute = 'home';
						_displayMainContent();						
					} else if(_currentRoute.match('track') != null) {
						var _id = _currentRoute.split("_")[1];
						_currentRoute = 'track';
						_displayMainContent(_id);
						delete _id;
					} else if(_currentRoute.match('thanks') != null) {
						var _id = _currentRoute.split("_")[1];
						_currentRoute = 'thanks';
						_displayMainContent(_id);
						delete _id;
					}
				break;
			}

		}

		var randomLoadingText;
		var RLTtexts = [
			'By listening to music, flowers can grow faster.',
			'A tune that gets stuck in your head is called an "earworm".',
			'It’s time to mix your own tracks!',
			'Your heartbeat changes and simulate the beats you listen to.'
		]
		var currRLT = '';
		function generateRandomLoadingText(){
			var n = RLTtexts.length;
			var i = Math.round( Math.random() * n );
			currRLT = i;
			$('.transparentBG .random-text').html(RLTtexts[i]);
			randomLoadingText = setInterval(function(){
				n = RLTtexts.length;
				i = Math.round( Math.random() * n );
				if (i == currRLT){
					if (i == 1){
						i++;
					}
					else {
						i--;
					}
				}
				currRLT = i;
				$('.transparentBG .random-text').html(RLTtexts[i]);
			},3000);
		}

		function _homeLoad() {
			SoundPlayer.end();
			ClickManager.close('tutorial');			
		}

		function _checkFirstLogin() {
			//get cookies user
			function getCookie() {
			    var name = "username=";
			    var ca = document.cookie.split(';');
			    for(var i=0; i<ca.length; i++) {
			        var c = ca[i];
			        while (c.charAt(0)==' ') c = c.substring(1);
			        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
			    }		
			}	

			var datauser = $('#contentdata').data('user');			
			if(getCookie()==undefined) {
				ClickManager.show('tutorial');
			}
			else {
				//console.log("ini adalah "+datauser);
			}

			//set cookies user				
			document.cookie="username="+datauser+"; expires=Thu, 18 Dec 2023 12:00:00 UTC";	
		}

		function _displayMainContent(id){
			if(!_isLoading) {
				_isLoading = true;
				_isLoaded = false;

				$('.transparentBG').addClass('layout');
				generateRandomLoadingText();

				SoundPlayer.end();

				$.ajax({
					url : _baseHref +'body/'+ _currentRoute + (id == undefined ? '' : '?id='+id),
					type : 'GET',
					dataType : 'text',										
					success : function(content){

						_mainWrapper.html(content);
						_isLoading = false;							
						_isLoaded = true;

						var me = _mainWrapper.find(_contentconfig);

						_config = {
							body_id : me.data('body_id'),
							_class : me.data('class'),
							backgroundtype : me.data('backgroundtype')							
						};

						_maindiv.attr('id',_config.body_id);
						_bodySelector.removeClass().addClass('body').addClass(_config._class).addClass(_config.backgroundtype);

						if(_initcallback[_currentRoute] && typeof _initcallback[_currentRoute] == 'function') _initcallback[_currentRoute]();

						_proposeCloseLoader();

					},
					error : function(){
						window.location.reload();
					}
				})

			}

		}

		function _proposeCloseLoader() {

			if(_isLoaded && _allsoundloaded) {								
				$('.transparentBG').removeClass('layout');
				clearInterval(randomLoadingText);
			}

		}

		function _reroute(route) {

			window.location.hash = route;

		}

		$(window).on('hashchange',function(){
			_checkRoute();
		})

		return {
			trigger : function(eventname) {
				switch(eventname) {
					case 'allsoundloaded' :

						_allsoundloaded = true;
						_proposeCloseLoader();

					break;
				}
			},
			reroute : function(route) {
				_reroute(route);
			},
			checkroute : function(){
				_checkRoute();
			}
		}
	})();

	SliderManager = (function(){

		var _selector = $('.slide-product'),
			_itemLength = $('.slide-product').find('.item').length,
			_countSlide = 5,
			_maxSlide = parseInt(_itemLength/5),
			_slideLength = 350,
			_currentIndex = 0;
			_isAnimating = false;

		function _init(){
			$('.slider .prev').on('click',function(){
				_prev();
			})

			$('.slider .next').on('click',function(){
				_next();
			})

			_selector = $('.slide-product');
			_itemLength = $('.slide-product').find('.item').length;
		}

		function _next() {
			if(!_isAnimating) {
				_isAnimating = true;
				
				var lastindex = _currentIndex;

				_currentIndex = ((_currentIndex+1) * _countSlide) > _itemLength ? _currentIndex : _currentIndex+1;

				lastindex !== _currentIndex ?
					setTimeout(function(){_isAnimating = false}, 800) :
					_isAnimating = false;

				_selector.css({
					marginLeft : -_currentIndex*_slideLength
				});
			}
		}

		function _prev() {
			if(!_isAnimating) {
				_isAnimating = true;
				
				var lastindex = _currentIndex;

				_currentIndex = _currentIndex-1 < 0 ? _currentIndex : _currentIndex-1;

				lastindex !== _currentIndex ?
					setTimeout(function(){_isAnimating = false}, 800) :
					_isAnimating = false;

				_selector.css({
					marginLeft : -_currentIndex*_slideLength
				});
			}
		}

		

		return {
			init : _init,
			prev : _prev,
			next : _next
		};
	})();

	// Sound manager
	SoundPlayer = (function(){
		var _VERSI = 'BARTIED',
			_MAXVOLUME = {
				'bass' : 0.8,
				'drum' : 0.8,
				'effect' : 0.3,
				'pad' : 0.6,
				'synth' : 0.3
			},
			_BARLENGTH = {
				"default" : 1
			},
			_dictionary = [],
			_instances = {},
			_instancesInterval = {},
			_instancesTimeout = {},
			_instancesStatus = {},
			_replaysTimeout = [],
	    	
	    	_actionrecorder = {},
	    	// We'll start playing the rhythm 100 milliseconds from "now"
			/*_tempo = 123.2, // BPM (beats per minute)*/
			_tempo = 124, // BPM (beats per minute)
			_eighthNoteTime = 60 / _tempo,//0.54525,//(240 / _tempo),

			_isStarted = false,
			_isRecording = false,
			_currentBar = 1,
			_barcounter = 0,

			_window = $(window),
			_beatelement,
			_beatfadetime = _eighthNoteTime * 1000 * 0.2,
			
	    	_endtime = null,
			_starttime = null,
	    	_beatInterval = null;

	    function dum(){
			
			_barcounter++;

			if(_isRecording) {
				if(!(_beatelement && $(_beatelement).length)) {
					_beatelement = $('#recordingboard .rec-wrapper .beat');
				}
				_beatelement.addClass('on');
			}

			setTimeout(function(){				
				if(_beatelement!=undefined) {_beatelement.removeClass('on bar')};
			},_beatfadetime);
			
			if(_barcounter >= 4) {
				//check start record

				_barcounter = 0;

				_currentBar++;

				console.log('bar',_currentBar);

				_window.trigger('bar');

				if(_isRecording) {
					_beatelement.addClass('bar');
				}
				
			} else {
				_window.trigger('beat');

			}
	    }

	    function barToTime(bar) {
	    	console.log('bartotime',((bar * (_eighthNoteTime*4000)) + _starttime) - new Date().getTime());
	    	return ((bar * (_eighthNoteTime*4000)) + _starttime) - new Date().getTime();
	    }

	    function record(id,action) {
	    	if(_isRecording) {
	    		_actionrecorder[new Date().getTime() - _starttime] = [id,action];	
	    	}
	    }

	    function animateBar(element,barlength,type) {
	    	var me = element.find('.bar span');

	    	var type = type || 'interval';

	    	switch(type) {
	    		case 'interval' :
	    			me.removeAttr("style");
	    			setTimeout(function(){
	    				me.css({
		    				width : '100%',
		    				'-webkit-transition' : (barlength*4*_eighthNoteTime - 0.01)+'s width linear',
		    				'-moz-transition' : (barlength*4*_eighthNoteTime  - 0.01)+'s width linear',
		    				'-o-transition' : (barlength*4*_eighthNoteTime  - 0.01)+'s width linear',
		    				'-ms-transition' : (barlength*4*_eighthNoteTime  - 0.01)+'s width linear',
		    				'transition' : (barlength*4*_eighthNoteTime  - 0.01)+'s width linear'
		    			})
	    			},100);
	    			console.log('animate for :',(barlength*4*_eighthNoteTime - 0.01)+'s');
	    		break;
	    	}

	    	delete me;
	    }

	    function end_clear(){
	    	_isStarted = false;
			_starttime = null;

			createjs.Sound.stop();

			clearInterval(_beatInterval);

			_.each(_instances,function(val,key){
				if(val) val.removeAllEventListeners();
				if(val.next) val.next.removeAllEventListeners();
			});

			_.each(_instancesTimeout,function(val,key){
				clearTimeout(val);
				delete _instancesTimeout[key];
			});

			_.each(_instancesInterval,function(val,key){
				clearInterval(val);
				delete _instancesInterval[key];
			});

			_.each(_replaysTimeout,function(val,key){
				clearTimeout(val);
				delete _replaysTimeout[key];
			});

			_.each(_instancesStatus,function(val,key){
				delete _instancesStatus[key];
			});

			_window.trigger('soundend');
	    }

		var _SoundPlayer = function() {
			var assetsPath = "assets/sound/",
				manifest = [],
				i = 0;

	        if($('#slider .item').length) {
				_.each($('#slider .item'),function(element,key,list){
		        	var me_type = $(element).data('type');
		        	_.each($('#dropzone .dropzone'),function(val){
		        		var _me_type = $(val).data('type');
		        		manifest.push({src:me_type+'/'+_me_type+'.mp3',id:me_type+'_'+_me_type});
		        		_dictionary.push(me_type+'_'+_me_type);
		        		delete _me_type;
		        	})
		        	delete me_type;
		        });
			/*} else if(($('.detail-wrapper').length)||($('.gallery-list').length)||($('.top-three').length)) {*/
			}

			var soundtype = [
				"bass",
				"drum",
				"pad",
				"effect",
				"synth"
			];

			var producttype = [
				"blackmint",
				"british",
				"butter",
				"cranberry",
				"lemon",
				"lemonlime",
				"lime",
				"mint",
				"raspberry",
				"spearmint",
				"walnut",
			];

			_.each(producttype,function(element,key,list){
	        	var me_type = element;
	        	_.each(soundtype,function(val){
	        		var _me_type = val;
	        		manifest.push({src:me_type+'/'+_me_type+'.mp3',id:me_type+'_'+_me_type});
	        		_dictionary.push(me_type+'_'+_me_type);
	        		delete _me_type;
	        	})
	        	delete me_type;
	        });
		

			createjs.FlashPlugin.swfPath = "assets/ori/js/libs/soundjs/";
			createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin, createjs.FlashPlugin]);

			createjs.Sound.alternateExtensions = ["mp3"];	// add other extensions to try loading if the src file extension is not supported
	        
	        createjs.Sound.addEventListener("fileload", createjs.proxy(soundLoaded, this)); // add an event listener for when load is completed
	        
	        createjs.Sound.registerManifest(manifest, assetsPath);

	        var _this = this;

	        function soundLoaded(event){

	        	_instances[event.id] = createjs.Sound.createInstance(event.id);

	        	//_instances[id] = createjs.Sound.createInstance(id);
	        	//
	        	i++;
	        	if(i >= _dictionary.length) {
	        		console.log('ALL SOUND LOADED',createjs.Sound.activePlugin.toString());
	        		
	        		_allsoundloaded = true;

	        		RouterManager.trigger('allsoundloaded');

	        		delete i;

	        	}	        	
	        	//console.log(event.id);
	        }

	        delete manifest;
	        delete assetsPath;
		}

		_SoundPlayer.prototype = {
			start : function(){
				console.log('================= soundplayer start called');
				if(_isStarted == 'ending') return false;

				if(!_isStarted){
					console.log('================= *START*');
					_isStarted = true;
					_actionrecorder = {};
					_currentBar = 1;
					_barcounter = 0;
					_starttime = new Date().getTime();

					createjs.Sound.setVolume(1);

					if(_VERSI == 'REALTIME'){
						
					} else {
						//dum();
						console.log('bar',_currentBar);
						_beatInterval = setInterval(dum,_eighthNoteTime*1000);
					}

					_window.trigger('soundstart');
				}
			},
			straightend : function(){
				if(_isStarted == true) {

					$('.product').find('.item').remove();
					$('.product').find('.dropzone').removeClass('occupied');

					end_clear();
					
				}
			},
			end : function(){

				console.log('END',_isStarted);

				record(null,'end');

				if(_isStarted == true) {
					
					_isStarted = 'ending';
					$('.product').find('.item').remove();
					$('.product').find('.dropzone').removeClass('occupied');

					var n = 1;

					var endinterval = setInterval(function(){

						n -= 0.02;
						createjs.Sound.setVolume(n);

						if(n <= 0) {

							clearInterval(endinterval);

							end_clear();

						}

					},20)

	        		_endtime = new Date().getTime();
				}
			},
			play : function(id, element){
				//return false;
				
				if(_isStarted == 'ending') return false;
				
				//start record				
				record(id,'play');	
				console.log('starttime',_starttime,'relativenow',(new Date().getTime() - _starttime));
							
				if(_instancesStatus[id] !== true) {

					console.log('PLAY :',id);

					console.log('================= *PLAY*');

					var _this = this;

					if(!_isStarted) this.start();

					var _newInstance = _instancesStatus[id] == 'fadingout' ? false : true;

					_instancesStatus[id] = true;

					console.log("IS NEW INSTANCE",_newInstance);

					if(_newInstance) {
						//play at next bar
						
						_instances[id].view = element;

						_instancesTimeout[id] = setTimeout(function(){
							//createjs.Sound.play(id, createjs.Sound.INTERRUPT_NONE, 0, 0, -1, 1);
							
							console.log('CURRENT BAR :',_currentBar);
							
							_instances[id].play({
								interrupt : createjs.Sound.INTERRUPT_NONE,
								volume : 0
							});

							_instances[id].playbar = _currentBar-1;							

							if(_BARLENGTH[id] == undefined) {
								_BARLENGTH[id] = Math.max(Math.round(_instances[id].getDuration() / (_eighthNoteTime*4000)),1);
								console.log(id,_BARLENGTH[id],_instances[id].getDuration(),(_eighthNoteTime*4000));
							}

							_instances[id].currentlyPlaying = 'main';

							_instances[id].removeAllEventListeners();
							_instances[id].addEventListener('complete',function(){
								console.log('FIRST INSTANCE OF',id,'FINISHED PLAYING, current bar:',_currentBar);
								_instances[id].currentlyPlaying = 'second';
								_this.playNext(id);
							});

							_this.playNext(id);							
							_this.fadeIn(id);
							
						}, barToTime(_currentBar)// _instancesStatus[id] == 'fadingout' ? barToTime(_BARLENGTH[id]*Math.ceil(_currentBar/_BARLENGTH[id])) : barToTime(_currentBar)						
						);
					} else {
						clearTimeout(_instancesTimeout[id]);
						clearInterval(_instancesInterval[id]);

						console.log('RESUMING :', _instances[id].currentlyPlaying);

						if(_instances[id].currentlyPlaying == 'main') {

							_instances[id].setVolume(_MAXVOLUME[id.split("_")[1]]);

							_instances[id].addEventListener('complete',function(){
								console.log('FIRST INSTANCE OF',id,'FINISHED PLAYING');
								_instances[id].currentlyPlaying = 'second';
								_this.playNext(id);
							});

						} else if(_instances[id].currentlyPlaying == 'second') {

							_instances[id].next.setVolume(_MAXVOLUME[id.split("_")[1]]);

							_instances[id].next.addEventListener('complete',function(){
								console.log("SECOND INSTANCE OF",id,"FINISHED PLAYING");
								_instances[id].currentlyPlaying = 'main';
								_this.playNext(id);
							});

						}

						_this.playNext(id,true);
					}
				}
			},
			stop : function(id) {
				record(id,'stop');
				
				if(_instancesStatus[id] == true) {

					console.log('stopping, currently playing :',_instances[id].currentlyPlaying);
					//remove animate
					//event.target.parent().find('.animate').remove();
					if(_instances[id].currentlyPlaying == undefined) {
						
						clearTimeout(_instancesTimeout[id]);
						_instancesStatus[id] = false;

						if(_instances[id].view) _instances[id].view.remove();

					} else {

						_instancesStatus[id] = 'fadingout';

						var _this = this;
						
						if(_instances[id]) _instances[id].removeAllEventListeners();
						if(_instances[id].next) _instances[id].next.removeAllEventListeners();

						if(_instances[id].currentlyPlaying == 'main') {
							_instances[id].next.stop();
						} else if(_instances[id].currentlyPlaying == 'second') {
							_instances[id].stop();
						}

						_instancesTimeout[id] = setTimeout(function(){
							_this.fadeOut(id)
						}, ( _currentBar+1 ) - 500 ) //barToTime(_instances[id].playbar + _BARLENGTH[id]*Math.ceil(((_currentBar) - _instances[id].playbar)/_BARLENGTH[id])) <= 500 ? 0 : barToTime(_instances[id].playbar + _BARLENGTH[id]*Math.ceil(((_currentBar) - _instances[id].playbar)/_BARLENGTH[id])) 
						//this.fadeOut(id);
						
						//clearInterval(_instancesInterval[id]);
					}

				}
			},
			playNext : function(id,isNow){
				var _this = this;

				console.log('playnext',isNow);

				//old with delay
				var nextbar = _instances[id].playbar + _BARLENGTH[id]*Math.ceil(((_currentBar+(isNow ? 0 : 1)) - _instances[id].playbar)/_BARLENGTH[id]);

				//new without delay
				//var nextbar = _BARLENGTH[id]*Math.ceil(((_currentBar+(isNow ? 0 : 1)) - _instances[id].playbar)/_BARLENGTH[id]);
				
				console.log('playnext',_instances[id].currentlyPlaying);
				//return false;
				if(_instances[id].currentlyPlaying == 'second') {
					_instances[id].play({
						interrupt : createjs.Sound.INTERRUPT_NONE,
						volume : _MAXVOLUME[id.split("_")[1]],
						delay : barToTime( nextbar )
					});
					console.log('first :delayto',nextbar,"current bar :",_currentBar);
				} else if(_instances[id].currentlyPlaying == 'main') {
					_instances[id].next = createjs.Sound.play(id, createjs.Sound.INTERRUPT_NONE, barToTime(nextbar), 0, 0, _MAXVOLUME[id.split("_")[1]]);

					_instances[id].next.addEventListener('complete',function(){
						console.log("SECOND INSTANCE OF",id,"FINISHED PLAYING");
						_instances[id].currentlyPlaying = 'main';
						_this.playNext(id);
					});
					console.log('secon :delayto',nextbar,"current bar :",_currentBar);
				}

				if(_instances[id].view) animateBar(_instances[id].view,_BARLENGTH[id]);
			},
			fadeIn : function(id) {
				var _i = 0;
				var _maxvolume = _MAXVOLUME[id.split("_")[1]];
				var obj = _instances[id].currentlyPlaying == 'main' ? _instances[id] : _instances[id].next;
				
				_instancesInterval[id] = setInterval(function(){
					_i++;
					if(obj)obj.setVolume(_maxvolume*(_i/20));
					if(_i>=20) {
						clearInterval(_instancesInterval[id]);
						_i = _maxvolume = null;
						delete _maxvolume;
						delete _i;
						delete obj;
					}
				},25);
			},
			fadeOut : function(id) {
				var _i = 20;
				var _maxvolume = _MAXVOLUME[id.split("_")[1]];
				var obj = _instances[id].currentlyPlaying == 'main' ? _instances[id] : _instances[id].next;

				if(obj == undefined) return false;
				
				_instancesInterval[id] = setInterval(function(){
					_i--;
					obj.setVolume(_maxvolume*(_i/20));
					if(_i<=0) {
						clearInterval(_instancesInterval[id]);

						if(_instancesStatus[id] == 'fadingout') {
							_instancesStatus[id] = false;
						}
						
						_i = _maxvolume = null;
						delete _maxvolume;
						delete _i;
						delete obj;
					}
				},25);
			},
			replay : function(replaydata) {				
				if(replaydata == undefined) var replaydata = _actionrecorder;				

				var _this = this;

				_.each(replaydata,function(val,key,list){
					_replaysTimeout.push(setTimeout(function(){
						_this[val[1]](val[0]);
					},key));
					console.log('REPLAY :',val[0],val[1],key);
				});
			},
			recording : function(){
				return _actionrecorder;
			},
			isStarted : function(){
				return _isStarted;
			},
			isRecording : function(recordstatus) {
				if(recordstatus === undefined) {
					return _isRecording
				}
				_isRecording = recordstatus;
				return _isRecording;
			},
			startTime : function(){
				return _starttime;
			}
		}

		return new _SoundPlayer();
	})();

	// Draggable
	Draggable = (function(){

		function _init(){
			var _template = '<div class="item {{TYPE}}" data-type="{{TYPE}}"><div class="bar"><span></span</div></div>',
				_hoverelement = $('#itemholder'),
				_draggedelement,
				_selectedelement,
				_selectedelementtype,
				_selectedelementparent,
				_sourcetype,
				_isDroppedOnDropzone = false,
				_isMoving = false,
				_isTypeSame = false,
				_productelement = $('.product');

			interact('.item').draggable({
				onmove: function (event) {
		           	_draggedelement.css({
		           		left : event.pageX,
		           		top : event.pageY
		           	})
		        }
			});

			interact('.product').dropzone({
				accept: '.item',
				overlap : 'pointer',
				ondropactivate: function (event) {
			        // add active dropzone feedback
			        _selectedelement = $(event.relatedTarget);
			        _selectedelementparent = _selectedelement.parent();
			        _selectedelementtype = _selectedelement.data('type');

			        _draggedelement = $(_template.replace(/{{TYPE}}/gi,_selectedelement.data('type')));
			        _hoverelement.html(_draggedelement);

			        _isDroppedOnDropzone = false;
			        _dropTargetElement = false;

			        if(_selectedelement.parent().is('.dropzone')) {
			        	_sourcetype = 'droppeditem';
			        	_selectedelement.removeClass('active').addClass('transparent');
			        } else {
			        	_sourcetype = 'selector';
			        	_selectedelement.css('visibility','hidden');
			        }

			    },
			    
			    ondropdeactivate: function (event) {
			    	
			    	var isOccupied = (_dropTargetElement && _dropTargetElement.is('.occupied')) || false,		    		
			    		isSameVariant = isOccupied ? (_dropTargetElement.data('currentType') == _selectedelementtype ? true : false) : false,
			    		isSameTarget = isOccupied ? (_dropTargetElement.html() == _selectedelementparent.html() ? true : false) : false,
			    		myelement = $(_template.replace(/{{TYPE}}/gi,_selectedelementtype));
			    	
			    	if(_sourcetype == 'selector') {
		    			_selectedelement.css('visibility','visible')
		    		} else {
		    			//DELETE ITEM AND STOP PLAYING MUSIC
		    			if(!isSameTarget) {

		    				SoundPlayer.stop(_selectedelementtype+'_'+_selectedelementparent.data('type'));

			    			var me = _selectedelement;
							me.css({
			    				'transition' : 'none',
			    				'webkitTransition' : 'none',
			    				'mozTransition' : 'none'
			    			}).fadeOut(300,function(){
			    				me.remove();
			    				delete me;
			    			});
			    			
			    			_dropTargetElement && _dropTargetElement.data('currentType','false');
			    			_selectedelementparent.removeClass('occupied');

		    			} else {
		    				_selectedelement.addClass('active').removeClass('transparent');	
		    			}
		    		}

		    		if(_isDroppedOnDropzone) {
		    			
		    			if(!isOccupied || (isOccupied && !isSameVariant)) {

		    				SoundPlayer.play(_selectedelement.data('type')+'_'+_dropTargetElement.data('type'),myelement);

			    			setTimeout(function(){_dropTargetElement.prepend(myelement).addClass('occupied');},50);

			    			_dropTargetElement.data('currentType',_selectedelementtype);

			    			if(isOccupied) {
			    				var me = _dropTargetElement.find('.item');

			    				SoundPlayer.stop(me.data('type')+'_'+_dropTargetElement.data('type'));

				    			me.css({
				    				'transition' : 'none',
				    				'webkitTransition' : 'none',
				    				'mozTransition' : 'none'
				    			}).fadeOut(300,function(){
				    				me.remove();
				    				delete me;
				    			});
			    			}

		    			}

		    		}

			    	setTimeout(function(){
			    		if(!SoundPlayer.isRecording() && !$('.dropzone').find('.item').length) {
					    	SoundPlayer.end();
					    }
			    	},300);

			    	_draggedelement.remove();
			    	_draggedelement = null;

			    	delete myelement;

			    }
			})

			interact('.dropzone').dropzone({
				accept : '.item',
				overlap : 'pointer',
			    ondragenter: function (event) {

			        $(event.target).addClass('hover');
					
			    },
			    ondragleave: function (event) {
			        
			        $(event.target).removeClass('hover');
			        
			    },
			    ondrop: function (event) {

			    	_isDroppedOnDropzone = true;

			    	$(event.target).removeClass('hover');

			    	_dropTargetElement = $(event.target);
			    },
			});
		}

		return {
			init : _init
		}
	})();

	ProgressManager = (function(){
		var _window = $(window),
			_endtime,
			_playtimer = false,
			_playtime = 0,
			_isStarted = false,
			_callback,
			_element = {
				progressbar : undefined,
				second : undefined,
				playbutton : undefined
			};

		function _updateview(){

			_element.second.text(_playtime+' s');
			_element.progressbar.css('width', (_playtime*100/_endtime) +'%');			
		}

		function _start(endtime,submissionid) {			
			if(!_isStarted && endtime !== undefined) {

				console.log('================= progressmanager start called',endtime);

				_isStarted = true;
				_element.playbutton.addClass('stop');

				_endtime = endtime;

				_playtime = 0;
				_playtimer = setInterval(function(){
					_playtime++;
					_updateview();
					//make progress bar smooth animate with divede 100
					if(_playtime >= _endtime) {
						_stop();
					}					
				}, 1000); // 1000);
				
				//ini yang lama
				/*if(submissionid !== undefined) {					
					$.ajax({
						url: 'api/user/counter',
						type: 'post',
						data: {submission_id: $('.playback').data('submissionid')},
					});
				}*/

				$.ajax({
					url: 'api/user/counter',
					type: 'post',
					data: {submission_id: $('.playback').data('submissionid')},
				});
			}			
		}

		function _stop(){
			if(_isStarted) {
				_isStarted = false;
				if(_playtimer) {
					clearInterval(_playtimer);
					_playtimer = false;
					_playtime = 0;
				}
				_element.playbutton.removeClass('stop');
				_updateview();
				if(_callback && typeof _callback == 'function') {
					_callback();
					_callback = false;
				}
			}
		}

		return {
			start : function(format){
				_element.progressbar = format.progressbar;
				_element.second = format.secondview;
				_element.playbutton = format.playbutton;
				
				_start(format.endtime,format.submissionid);

				return this;
				
			},
			stop : _stop,
			progresselement : function(element) {
				if(element === undefined) {					
					return _element.progressbar;
				}
				_element.progressbar = element;				
				return this;
			},
			buttonelement : function(element) {
				if(element === undefined) {
					return _element.playbutton;
				}
				_element.playbutton = element;
				return this;
			},
			secondelement : function(element) {
				if(element === undefined) {
					return _element.second;
				}
				_element.second = element;
				return this;
			},
			isStarted : function(){
				return _isStarted;
			},
			onComplete : function(callback) {
				_callback = callback;

				return this;
			}
		}
	})();

	ClickManager = (function(){
		var _window = $(window),
			_playtimer,
			_playtime,
			_record = {
				result : null,
				starttimer : false,
				intervaltimer : false,
				maxtime : 60,
			};

		function stringify(recording) {
			return JSON.stringify({recordResult : recording});
		}

		function saverecord(form) {		
			var formData;
			if($('input[name=title]').val()) {
				formData = {recordData: $('input[name=recordData]').val(), title: $('input[name=title]').val()};
			}
			else {
				formData = {recordData: $('input[name=recordData]').val(), title: 'untitled'};
				//return false;
			}
			$.ajax({			
	           	type: "POST",
	           	url: "api/user/submit-music",           
	           	contentType: "application/json",
        		dataType: "json",
	           	data: JSON.stringify(formData),
	           	success: function(data)
	           	{
	           		console.log(data);
	           		if (data.status == 'succeed') 
	           		{
	               		window.location.hash = '#thanks_'+data.submission_id;
	       			}
				},
				error: function(){

				}
			});		
		}

		function startplayback(format) {
			if(!ProgressManager.isStarted() && SoundPlayer.isStarted() == false) {
				ProgressManager.start({
					progressbar : format.progressbar,
					secondview : format.secondview,
					playbutton : format.playbutton,
					endtime : format.endtime
				}).onComplete(function(){
					stoprecord();
				});
				SoundPlayer.start();
				SoundPlayer.replay(format.recording);

				if(format.submissionid) {
					$.ajax({
						url: 'api/user/counter',
						type: 'post',
						data: {submission_id: format.submissionid},
					})
				}

			}
		}

		function stopplayback() {			
			if(ProgressManager.isStarted() && SoundPlayer.isStarted()) {				
				ProgressManager.stop();
				SoundPlayer.straightend();
			}
		}

		function stoprecord(){
			if(SoundPlayer.isRecording()) {

				if(_record.starttimer) {
					clearTimeout(_record.starttimer);
					_record.starttimer = false;
				}

				SoundPlayer.end();
				SoundPlayer.isRecording(false);
				ProgressManager.stop();

				_record.result = SoundPlayer.recording();

				$('#savingboard').show();
				$('#recordingboard').hide();
				
				$('input[name=recordData]').val(stringify(_record.result));
				
			}
		}

		function startrecord(){
			//reset dropzone
			if(!ProgressManager.isStarted() && !SoundPlayer.isRecording()) {

				if(SoundPlayer.isStarted() == true) {
					SoundPlayer.end();
				}

				$('.transparentBG').addClass('active record-prepare');

				$('.countShow').addClass('down');

				_record.starttimer = setTimeout(function(){

					SoundPlayer.isRecording(true);
					
					$('.countShow').removeClass('down');

					$('.transparentBG').removeClass('active record-prepare');

					ProgressManager.start({
						progressbar : $('#recordingboard .front'),
						secondview : $('#recordingboard .time-wrapper'),
						playbutton : $('#recordingboard .record'),
						endtime : _record.maxtime
					}).onComplete(function(){
						stoprecord();
					});

					SoundPlayer.start();

				},4500);

			}

		}

		$('body').on('click','.tutorial .how',function(){
			ClickManager.show('tutorial');
			return false;
		}).on('click','.js-close-privacy',function(){
			ClickManager.close('privacy');
			return false;
		}).on('click','.js-close-syarat',function(){
			ClickManager.close('syarat');
			return false;
		}).on('click','.js-close-reload',function(){
			ClickManager.close('reload');
			return false;
		}).on('click','.js-close-tutorial',function(){
			ClickManager.close('tutorial');
			return false;
		}).on('click','.js-action-record',function(){
			if(!SoundPlayer.isRecording()) {
				startrecord();
			} else {
				stoprecord();
			}
			return false;
		}).on('click','.js-action-cancelrecord',function(){
			$('#saveInput input[name="title"]').val('');
			$('#savingboard').hide();
			$('#recordingboard').show();
			stopplayback();
			return false;
		}).on('click','.js-action-saverecord',function(){
			saverecord($('#saveInput'));
			return false;
		}).on('click','.js-action-play',function() {
			if(!SoundPlayer.isStarted()) {				
				var _this = $(this),
					recording = _this.data('recording'),
					endtime;

				if(recording == undefined || _this.data('progressbar') == undefined || _this.data('secondview') == undefined) return false;

				if(recording && recording == 'lastrecording') {
					_.find(_record.result,function(val,key){
						if(val[1] == 'end') endtime = parseInt(key)/1000;
						return val[1] == 'end';
					})
				} else if(recording) {
					_.find(recording.recordResult,function(val,key){
						if(val[1] == 'end') endtime = parseInt(key)/1000;
						return val[1] == 'end';
					})
				}

				startplayback({
					progressbar : $(_this.data('progressbar')),
					secondview : $(_this.data('secondview')),
					playbutton : _this,
					endtime : endtime,
					submissionid : _this.data('submissionid'),
					recording : recording == 'lastrecording' ? _record.result : recording.recordResult
				});

			} else {				
				stopplayback();
			}
		}).on('click','.js-validateform-register',function(){
			ClickManager.validateForm('register');
			return false;		
		}).on('click','.js-show-syarat',function(){
			ClickManager.show('syarat');
			return false;
		}).on('click','.js-show-privacy',function(){
			ClickManager.show('privacy');
			return false;
		}).on('click','.js-fbshare',function(){
			ClickManager.fbshare($(this));
			return false;
		}).on('click','.js-close-detail',function(){
			stopplayback();
			RouterManager.reroute('gallery');
			return false;
		})


		return {
			stopplayback : stopplayback,
			fbshare : function(me) {		
		   		FB.ui({
		   			method: 'feed',
				    picture: 'http://Soundofnature.originalsource.co.id/assets/img/logo-fb.png',
				    name: me.data('title'),
				    caption: 'Soundofnature.originalsource.co.id',    		
				    description: $('.playground-detail').length ? "Ini salah satu karya paling intens di Sound of Nature by Original Source, dengerin deh Track berikut!" : 'Cool! Baru saja nge-mix suara yang intense dari varian Original Source, buruan coba deh!',
		    		link: 'Soundofnature.originalsource.co.id/track_'+me.data('detail'),
		    	});
			},
			playDetail : function(event) {		
				RouterManager.reroute('track_'+$(event.target).data('submissionid'));
			},
			show : function(content) {
				switch(content) {
					case 'tutorial' :
						setTimeout(function(){$('.wrapper-tutorial').addClass('active')}, 100);
						$('.transparentBG').addClass('active');
						$('.transparentBG').show().animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 1
										}); 
								    },
								    duration:'500'
							}, 
						'500');
						break;
					case 'privacy' :
						$('.logo').addClass('bottom');
						$('.privacy').show().animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 1
										}); 
								    },
								    duration:'300'
							}, 
						'300');
						$('.transparentBG').show().animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 1
										}); 
								    },
								    duration:'500'
							}, 
						'500');
						$('.tutorial').children('div').css('z-index', 1);										
						break;
					case 'syarat' :
						$('.logo').addClass('bottom');
						$('.syarat').show().animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 1
										}); 
								    },
								    duration:'300'
							}, 
						'300');
						$('.transparentBG').show().animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 1
										}); 
								    },
								    duration:'500'
							}, 
						'500');
						$('.tutorial').children('div').css('z-index', 1);										
						break;
					case 'reload' :
						$('.transparentBG').text('');			
						
						$('.reloadnotif').show().animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 1
										}); 
								    },
								    duration:'300'
							}, 
						'300');
						$('.transparentBG').show().animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 1
										}); 
								    },
								    duration:'500'
							}, 
						'500');
						$('.tutorial').children('div').css('z-index', 1);										
						break;
				}
			},
			close : function(content) {
				switch(content) {
					case 'tutorial' : 
						setTimeout(function() {$('.wrapper-tutorial').removeClass('active');}, 50)
						setTimeout(function() {$('.transparentBG').removeClass('active');}, 500);
						// $('.transparentBG').animate(
						// 	{textIndent: 0}, 
						// 	{
						// 		step: function() {
						// 		      	$(this).css({
						// 					opacity: 0
						// 				}); 
						// 		    },
						// 		    duration:'500'
						// 	}, 
						// '500');
						break;
					case 'privacy' : 
						$('.logo').removeClass('bottom');
						setTimeout(function() {$('.privacy').hide()}, 100);
						$('.transparentBG').animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 0
										}); 
								    },
								    duration:'500'
							}, 
						'500');
						setTimeout(function() {							
							$('.transparentBG').hide();
							$('.tutorial').children('div').css('z-index', 21);							
						}, 500);
						break;
					case 'syarat' : 
						$('.logo').removeClass('bottom');
						setTimeout(function() {$('.syarat').hide()}, 100);
						$('.transparentBG').animate(
							{textIndent: 0}, 
							{
								step: function() {
								      	$(this).css({
											opacity: 0
										}); 
								    },
								    duration:'500'
							}, 
						'500');
						setTimeout(function() {							
							$('.transparentBG').hide();
							$('.tutorial').children('div').css('z-index', 21);							
						}, 500);
						break;
					case 'reload' : 
						window.location.reload();
						break;
				}
			},
			validateForm : function(type) {	
				switch(type) {
					case 'register' :
						var name = $("input[name='name']"),
						email = $("input[name='email']"),
						iagree = $("input[name='iagree']"),		
						validateEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					
						//validate input
						if(name.val()=='') {			
							$('.message').text('Anda belum mengisi nama');
						}
						else if(email.val()=='') {			
							$('.message').text('Anda belum mengisi email');
						}
						else if(validateEmail.test(email.val())==false) {
							$('.message').text('Format Email Salah');				
						}
						else if($('.checked').length==false) {
							$('.message').text('Anda belum menyetujui syarat dan ketentuan yang berlaku');	
						}
						else {
							$('.message').text('');	
						}
						
						if($('.message').text().length>0) {
							return false;	
						}				
						else {

							$.ajax({
								url: 'api/user/register',
								type: 'POST',
								dataType: 'json',
								data: $('#formRegister').serialize(),
								success: function(data) {
									if (data.status == 'succeed')
										RouterManager.reroute('like');
									else
										RouterManager.reroute('register');
								}
							});
							
						}
					break;					
				}					
			}
		}
	})();

	$(window).on("focus blur", function(e) {
		if(SoundPlayer.isStarted()==true) {
			if($('#playground').length) {
	  			ClickManager.show('reload');
	  		}
	  		else if($('.playground-detail').length) {
	  			ProgressManager.stop();
	  		}
	  		SoundPlayer.straightend();
		}  		
	})
	
	return {
		init:function(){
			customCheckbox();
			centerImages('.container .box');
			gallerySearch();		
		},
		RouterManager : RouterManager
	}
})();

$(document).ready(function(){
	
	$("input").forceNumeric();
	
	xion.init();

	$(".syarat-wrapper").mCustomScrollbar({
		scrollButtons: {
            enable: true,
        },
		theme:"rounded",
		advanced: {
            updateOnContentResize: true,
            updateOnBrowserResize: true
        }
	});

	$(".privacy-wrapper").mCustomScrollbar({
		scrollButtons: {
            enable: true,
        },
		theme:"rounded",
		advanced: {
            updateOnContentResize: true,
            updateOnBrowserResize: true
        }
	});
	xion.RouterManager.checkroute();
});

$(window).on('load',function(){
	$(window).focus();
})
